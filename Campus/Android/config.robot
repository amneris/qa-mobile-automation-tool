*** Settings ***
Library  AppiumLibrary
Library  OperatingSystem
Library  String
Library  Collections

Resource    variables.robot
Resource    ../../startDevice.robot
Resource	android_Keywords.robot
Resource	../../users.robot



*** Keywords ***

My Foo Bar Keyword
    [Documentation]    Does so and so
    [Arguments]        ${arg1}
    Do this
    Do that
    [Return]           Some value