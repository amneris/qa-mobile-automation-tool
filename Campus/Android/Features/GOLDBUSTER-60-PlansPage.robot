*** Settings ***
Resource    ../config.robot
#Resource    ../../../startDevice.robot
Documentation  This file contains the tests that use different pages of the Mobile Application.
Library  AppiumLibrary
Library  String

#Suite Set Up        I start the ABA application
Test Set Up         I start the ABA application
Test Tear Down      close application
#                        Run Keywords
#...				    Run Keyword If Test Failed    close application   AND
#...                 Run Keyword If Test Failed	get screen shot
#Suite Tear Down	    Reset application

*** Variables ***


*** Test Cases ***


User should be able to see the new plan page from main menu
    [Documentation]     GOLDBUSTER-60 The bug was solved in the ticket: ABAANDROID-739
    [Tags]  android     smoke    GOLDBUSTER-60
    Given I login in the application    ${ES}   ${PASS}
    When I go to the plan page using the menu
    then new plan page should be displayed
    then Choose an ABA Premium subscription tittle should be present
    And list prices should be present
    then Monthly plan should be present
    then SixMonthly plan should be present
    and Annual plan should be present
    Recurring billing cancel anytime message should be present


User should be able to see the new plan page from user account
    [Documentation]     GOLDBUSTER-60 from account
    [Tags]  android     medium      GOLDBUSTER-60
    Given I login in the application    ${CA}   ${PASS}
    When I go to My Account area using the menu
    I tap on view prices from my account
    And Monthly plan should be present
    And SixMonthly plan should be present
    And Annual plan should be present


Beginners- old free users should see - the new plan page when they try to access to a blocked section
    [tags]              android     smoke    GOLDBUSTER-60
    [Documentation]     GOLDBUSTER-60 new pan page from a blocked sections - beginners
    Given I login in the application  ${ES}   ${PASS}
    When I change of level   Beginners
    When I tap on a unit    3
    And I tap on section    Write
    And Layout of exclusively for students with ABA premium should be displayed
    then I tap on View prices
    then new plan page should be displayed

Lower intermediate- old free users should see -the new plan page when they try to access to a blocked section
    [tags]              android     medium    GOLDBUSTER-60
    [Documentation]     GOLDBUSTER-9 new pan page from a blocked sections - lower int
    Given I login in the application  ${ES}   ${PASS}
    When I change of level   Lower Intermediate
    When I tap on a unit  26
    And I tap on section    Write
    And Layout of exclusively for students with ABA premium should be displayed
    Then I tap on View prices
    And new plan page should be displayed


intermediate- old free users should see - the new plan page when they try to access to a blocked section
    [tags]              android     medium    GOLDBUSTER-60
    [Documentation]     GOLDBUSTER-60 new pan page from a blocked sections - intermediate
    Given I login in the application  ${ES}   ${PASS}
    When I change of level   Intermediate (B1)
    then I tap on a unit   50
    And I tap on section    Write
    And Layout of exclusively for students with ABA premium should be displayed
    Then I tap on View prices
    And new plan page should be displayed


advanced- old free users should see - the new plan page when they try to access to a blocked section
    [tags]              android     main    GOLDBUSTER-60
    [Documentation]     GOLDBUSTER-60 new pan page from a blocked sections s- advanced
    Given I login in the application  ${ES}   ${PASS}
    When I change of level   Advanced
    then I tap on a unit   98
    And I tap on section    Write
    And Layout of exclusively for students with ABA premium should be displayed
    Then I tap on View prices
    And new plan page should be displayed



new registered users should be able to see the option Why make you premium page when they try to access to a blocked section
    [tags]              android     main    GOLDBUSTER-60    QAEnv
    [Documentation]     GOLDBUSTER-60 new users
    Given a new user registered in the app   GOLDBUSTER-60
    Then The user is on the main level page
    Then I select a level  Advanced
    When I go to My Account area using the menu
    Then I go to the plan page using the menu
    And new plan page should be displayed
    Plan page should contain a rate plan by    MONTHLY


