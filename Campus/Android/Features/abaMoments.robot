*** Settings ***
Documentation  This file contains the tests that use different pages of the Mobile Application.
Library  AppiumLibrary
Library  String

Resource    ../config.robot

Test Set Up         I start the ABA application
Test Tear Down      close application

*** Variables ***

*** Test Cases ***
Free users should not be able to see the ABA Moments
#    [Tags]  abaMoment   smoke  pendingImplement
    Given I login in the application with premium user
    When I click in the menu icon
    Then I should not see the ABA Moments option in the menu

New users should not be able to see the ABA Moments
#    [Tags]  abaMoment   main   pendingImplement
    Given a new user registered in the app  ABA Moments
    When I click in the menu icon
    Then I should not see the ABA Moments option in the menu

As I spanish user, I should see the aba activities in Spanish
    [Tags]  abaMoment  smoke   spanish
    [Documentation]     The code is on the branch.
    Given I login in the application    ${MX}   ${PASS}
    When On main menu I tap on  ABAMOMENT
    Then I should see in the aba moments description        Eres capaz de aprender 10 palabras
    And I should see the colors activity with the name      Colores
    And I should see the animals activity with the name     Animales

I should be redirected to the current unit after complete the activity and I click in the pop-up
    [Tags]  abaMoment   medium  taskPending
#    Given I login in the application with premium user that is in the unit 2
    Given I login in the application with premium user
    When On main menu I tap on  ABAMOMENT
    And I start the colors activity
    And I complete this amount of aba activities    10
    And I accept the ABA Moment confirmation pop-up
    Then I am in the unit   Unit 2      A New Friend

I should be redirected to the current unit after complete the activity and I do not click in the pop-up
    [Tags]  abaMoment   main   taskPending
#    Given I login in the application with premium user that is in the unit 2
    Given I login in the application with premium user
    When On main menu I tap on  ABAMOMENT
    And I start the colors activity
    And I complete this amount of aba activities    10
    And I wait until ABA Moment confirmation pop-up does not exist
    Then I am in the unit   Unit 2      A New Friend

I can stay in the ABA Moment section after an activity is completed
    [Tags]  abaMoment   main
    Given I login in the application with premium user
    When On main menu I tap on  ABAMOMENT
    And I start the colors activity
    And I complete this amount of aba activities    10
    And I close the ABA Moment confirmation pop-up
    Then I should be in the ABA Moment menu

I should lost the progress when the ABA Moment activity is not completed
    [Tags]  abaMoment   smoke
    Given I login in the application with premium user
    When On main menu I tap on  ABAMOMENT
    And I start the colors activity
    And I complete this amount of aba activities    2
    And I close the activity
    And I start the colors activity
    Then I should be in the question    0

I can do two activities of ABA Moment one after another
    [Tags]  abaMoment   main
    Given I login in the application with premium user
    When On main menu I tap on  ABAMOMENT
    And I start the colors activity
    And I complete this amount of aba activities    10
    And I close the ABA Moment confirmation pop-up
    And I start the animals activity
    And I complete this amount of aba activities    10
    And I close the ABA Moment confirmation pop-up
    Then I should be in the ABA Moment menu

I should not change the answer if I select an wrong answer
    [Tags]  abaMoment   medium
    Given I login in the application with premium user
    When On main menu I tap on  ABAMOMENT
    And I start the colors activity
    And I select a wrong answer
    Then I should be in the question    0
    And I complete this amount of aba activities    1
    And I should be in the question    1

I should be redirected to the abamoment menu when I close an activity
    [Tags]  abaMoment   main
    Given I login in the application with premium user
    When On main menu I tap on  ABAMOMENT
    And I start the colors activity
    And I close the activity
    Then I should be in the ABA Moment menu