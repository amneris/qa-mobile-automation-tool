*** Settings ***
Resource    ../config.robot
#Resource    ../../../startDevice.robot
Documentation  This file contains the tests that use different pages of the Mobile Application.
Library  AppiumLibrary
Library  String

#Suite Set Up        I start the ABA application
Test Set Up         I start the ABA application
Test Tear Down      close application
#                        Run Keywords
#...				    Run Keyword If Test Failed    close application   AND
#...                 Run Keyword If Test Failed	get screen shot
#Suite Tear Down	    Reset application

*** Variables ***


*** Test Cases ***


After user finish an abaFilm an improved message of a teacher should be shown
    [tags]   GOLDBUSTER-111 IN PROGRESS
    Given a new user registered in the app  GOLDBUSTER-111
    then User should be inside of Menu levels
    when I select a level  Beginners
    And I tap on section   ABA Film
    then I start the aba film class
    And I wait until the video is completed  2
    Then improved message of a teacher should be shown
    And the popup should have a message about Carry on studying with the section Speak
    And continue button should be shown in popup
    And exit button should be present

user should be redirected to the next section by tapping on "seguir con la unidad [actualUnit]" button
    [tags]   GOLDBUSTER-111 IN PROGRESS
    Given a new user registered in the app   GOLDBUSTER-111 IN PROGRESS
    when I select a level  Beginners
    And I tap on section   ABA Film
    then I start the aba film class
    And I wait until the video is completed  2
    When I complete the AbaFilm class
    then improved message of a teacher should be shown
    Then I tap on continue button
    #And Users are redirected to the next section
    #I start the speak class

#user should be redirected to the next section after waiting 3 seconds with no action.
#    [tags]   GOLDBUSTER-111 IN PROGRESS
#    Given a new user registered in the app   GOLDBUSTER-111 IN PROGRESS
#    Given logged user should be on Unit 49
#    Then I tap on AbaFilm button
#    When I complete the AbaFilm class
#    then improved message of a teacher should be shown
#    wait 3 seconds
#    Users are redirected to the next section
#Users are easily able to stop the redirection closing the feedback lightbox.
#    [tags]   GOLDBUSTER-111 IN PROGRESS
#    Given a new user registered in the app   GOLDBUSTER-111 IN PROGRESS
#    Given logged user should be on Unit 97
#    Then I tap on AbaFilm button
#    When I complete the AbaFilm class
#    then improved message of a teacher should be shown
#    tap on close icon
#    message about progress should be present


