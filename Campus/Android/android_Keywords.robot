*** Settings ***
Documentation  This file contains the keywords used in the Android application.
Library  AppiumLibrary
Library  OperatingSystem
Library  String
Library  Collections

# Files that are called
Resource    ../../users.robot
Resource    ../../globalKeywords.robot
Resource    variables.robot

*** Variables ***
${abamoments-file-answers}              TXT Files/abamoments.txt
${assessment-file-answers-unit-1}       TXT Files/assessment-unit-1.txt

${question-number}      1
*** Keywords ***

a random number
	[Return]  ${time_stamp}
      ${secs}=  Get Time  epoch
      ${time}=  Get Time
      ${time_stamp}=  Convert To String     ${secs}
      Set Suite Variable	${random}	${time_stamp}

I scroll to the text
    [Documentation]  This keyword moves the screen down until the text sent appears in the screen, the scrool is executed no more than 5 times.
    [Arguments]  ${text}
    : FOR    ${i}    IN RANGE    0    5
    \    ${value}    Run Keyword And Return Status    page should contain text    ${text}
    \    Run keyword if    ${value} == False    Swipe    15    600    15    100
    \    Run Keyword If    ${value} == True     Exit For Loop
    \    ${i}    Set Variable    ${i}+1

I scroll to the element
    [Documentation]  This keyword moves the screen down until the element sent appears in the screen, the scrool is executed no more than 5 times.
    [Arguments]  ${element}
    : FOR    ${i}    IN RANGE    0    5
    \    ${value}    Run Keyword And Return Status    Wait Until Page Contains Element    ${element}
    \    Run keyword if    ${value} == False    Swipe    15    600    15    100
    \    Run Keyword If    ${value} == True     Exit For Loop
    \    ${i}    Set Variable    ${i}+1

Swipe UP
        [Arguments]  ${element}
        : FOR    ${i}    IN RANGE    0    5
        \    ${value}    Run Keyword And Return Status    Wait Until Page Contains Element    ${element}
        \    Run keyword if    ${value} == False    Swipe    25    400    25    900
        \    Swipe    25    400    25    800
        \    Run Keyword If    ${value} == True     Exit For Loop
        \    ${i}    Set Variable    ${i}+1

Swipe DOWN
        [Arguments]  ${element}
        : FOR    ${i}    IN RANGE    0    5
        \    ${value}    Run Keyword And Return Status    Wait Until Page Contains Element    ${element}
        \    Run keyword if    ${value} == False    Swipe    25    600    25    100
        \    Swipe    15    600    15    100
        \    Run Keyword If    ${value} == True     Exit For Loop
        \     ${i}    Set Variable    ${i}+1

####--- LOGIN PAGES ---####
#- Initial Page
I click in the log-in button in the login main window
    sleep  5s
    ${passed} =	    Run Keyword And Return Status	element should be enabled   id=com.abaenglish.videoclass:id/loginTextView
    log     ${passed}
    Run Keyword If	${passed} == True   I click in the log-in button of mobile

    ${passed} =	    Run Keyword And Return Status	element should be enabled   id=com.abaenglish.videoclass:id/loginTextView
    log     ${passed}
    Run Keyword If	${passed} == True   I click in the log-in button of mobile

I click in the register now button in the login main window
    sleep  3s
    ${passed} =	    Run Keyword And Return Status	element should be enabled   id=com.abaenglish.videoclass:id/registerButton
    Run Keyword If	${passed} == True   I click in the register now button of mobile

    ${passed} =	    Run Keyword And Return Status	element should be enabled   id=com.abaenglish.videoclass:id/registerButton
    Run Keyword If	${passed} == True   I click in the register now button of mobile

I should be in initial window of the application
    element should be enabled   id=com.abaenglish.videoclass:id/loginTextView
    element should be enabled   id=com.abaenglish.videoclass:id/loginTextView

I click in the log-in button of mobile
    tap    id=com.abaenglish.videoclass:id/loginTextView

I click in the register now button of mobile
    tap     id=com.abaenglish.videoclass:id/registerButton


#- Login page (cases to Login in the page)
I login in the application with free user
    I login in the application  ${FREE USER-EMAIL}   ${FREE USER-PASSWORD}

I login in the application with premium user
    I login in the application  ${PREMIUM USER-EMAIL}  ${PREMIUM USER-PASSWORD}

I login in the application
    [Arguments]  ${email}   ${password}
    I click in the log-in button in the login main window
    I fill the user email field in the login        ${email}
    I fill the user password field in the login     ${password}
    I click in the log-in button
    sleep  8s
#    wait until page contains    YOUR ACCOUNT
    click a point  400  600



I fill the user email field in the login
    [Arguments]  ${email}
    wait until page contains element  id=com.abaenglish.videoclass:id/emailEditTextInput
    input text  id=com.abaenglish.videoclass:id/emailEditTextInput      ${email}

I fill the user password field in the login
    [Arguments]  ${password}
    input text  id=com.abaenglish.videoclass:id/passwordEditTextInput    ${password}

I click in the log-in button
    wait until page contains element    id=com.abaenglish.videoclass:id/loginActionButton
    tap  id=com.abaenglish.videoclass:id/loginActionButton

I click in the forgot password button
    tap     id=com.abaenglish.videoclass:id/loginChangePasswordButton

The user is in the log-in page
    wait until page contains element  id=com.abaenglish.videoclass:id/emailEditTextInput
    element should be enabled   id=com.abaenglish.videoclass:id/emailEditTextInput


#- Create user page (cases to create users in the page)
I create an user and I go to the menu of the level
    [Arguments]     ${level-name}
    I create an user with random email
    I select the level after registration   ${level-name}
    The user is on the main course page
    I tap in the back button in the course page

I create an user and I go to the level
    [Arguments]     ${level-name}
    I create an user with random email
    I select the level after registration   ${level-name}
    The user is on the main course page

a new user registered in the app
    [Arguments] 	${FEATURENAME}
    Given a random number
    I go to the creation user page
    I fill the user name field in creation account          MobileTest ${FEATURENAME}
    I fill the user email field in creation account         qastudent+mob${random}@gmail.com
    I fill the user password field in creation account      eureka
    Set Suite Variable	${NEWUSEREMAIL}	qastudent+mob${random}@gmail.com
    Set Suite Variable	${USERNAME}    MobileTest ${FEATURENAME}
    Sleep   1s
    And I click in the registor now button
    Sleep   3s

I go to the creation user page
    I click in the register now button in the login main window
    wait Until Page Contains Element    id=com.abaenglish.videoclass:id/nameEditTextInput

I create an user with random email
    I go to the creation user page
    I generate an random email
    I create an user with following data    ${RANDOM USER-EMAIL}    ${FREE USER-PASSWORD}

I create an user with following data
    [Arguments]     ${user-email}    ${user-password}
    I fill the user name field in creation account      qa-test
    I fill the user email field in creation account     ${user-email}
    I fill the user password field in creation account  ${user-password}
    Run Keyword And Return Status   Hide Keyboard
    I click in the registor now button
    sleep   2s
    wait until page contains element    id=com.abaenglish.videoclass:id/top_text

I create an accont with the free user email
    I fill the user name field in creation account      qa-test
    I fill the user email field in creation account     ${FREE USER-EMAIL}
    I fill the user password field in creation account  test12345
    Run Keyword And Return Status   Hide Keyboard
    I click in the registor now button

I fill the user name field in creation account
    [Arguments]     ${user-name}
    input Text  id=com.abaenglish.videoclass:id/nameEditTextInput     ${user-name}

I fill the user email field in creation account
    [Arguments]     ${user-email}
    input Text  id=com.abaenglish.videoclass:id/emailEditTextInput    ${user-email}

I fill the user password field in creation account
    [Arguments]     ${user-password}
    input Text  id=com.abaenglish.videoclass:id/passwordEditTextInput   ${user-password}

I click in the registor now button
    tap     id=com.abaenglish.videoclass:id/registerActionButton

    #- These keywords are for the pop-up when users are registered in the dddbb
The log-in pop-up redirects the user the to the Login Page
    tap     id=com.abaenglish.videoclass:id/snackbar_action
    The user is in the log-in page


#- Forgot password page
I go to the recover password page
    I click in the log-in button in the login main window
    I click in the forgot password button

I recover the password for the email
    [Arguments]  ${email}
    I fill the email to recover the password       ${email}
    I click in the recover password button

I fill the email to recover the password
    [Arguments]  ${email}
    input text  id=com.abaenglish.videoclass:id/changePassword    ${email}

I click in the recover password button
    tap  id=com.abaenglish.videoclass:id/changePasswordSend

The pop-up to recover the password has valid message
    wait until page contains element  id=com.abaenglish.videoclass:id/contentPanel
    element should be enabled   id=com.abaenglish.videoclass:id/contentPanel

The pop-up to recover the password can be closed
    tap  id=android:id/button1
    wait until page contains element  id=com.abaenglish.videoclass:id/emailEditTextInput
    element should be enabled  id=com.abaenglish.videoclass:id/emailEditTextInput

####------------- After this line, the keywords are for actions inside the app after login. ---####

####--- MENU ---####
On main menu I tap on
   [Arguments] 	${MAIN-MENU-OPTION}
   I click in the menu icon
   wait until page contains element   xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${MAIN-MENU-OPTION}')]
   Tap   xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${MAIN-MENU-OPTION}')]

I go to My Account area using the menu
    I click in the menu icon
    I click in the My Account button
    sleep  3s

I go to Levels area using the menu
    I click in the menu icon
    sleep  1s
    I click in the Levels button
    I am on the main level page

I go to the plan page using the menu
    I click in the menu icon
    sleep  1s
    I click in the view prices button

I go to the contact and help page using the menu
    I click in the menu icon
    sleep  1s
    I click in the contact and help button

I click in the menu icon
    wait until page contains element    id=com.abaenglish.videoclass:id/toolbarLeftButton
    tap     id=com.abaenglish.videoclass:id/toolbarLeftButton

I click in the My Account button
    wait until page contains element    xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[1]
    tap     xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[1]

I click in the Levels button
    wait until page contains element    xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[3]
    tap     xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[3]

The menu icon should be visible
    wait until page contains element    id=com.abaenglish.videoclass:id/toolbarLeftButton
    element should be enabled   id=com.abaenglish.videoclass:id/toolbarLeftButton

I click in the view prices button
    wait until page contains element    id=com.abaenglish.videoclass:id/premiumButton_menu
    tap     id=com.abaenglish.videoclass:id/premiumButton_menu

I click in the contact and help button
    wait until page contains element    xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[6]
    tap     xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[6]


####--- MY ACCOUNT ---####
I change my password writting new one
    [Arguments]  ${old-password}    ${new-password}
    wait until page contains element    id=com.abaenglish.videoclass:id/oldPasswordEditTextInput
    I fill the old password             ${old-password}
    I fill the first new password       ${new-password}
    I fill the second new password      ${new-password}
    Run Keyword And Return Status   Hide Keyboard
    I click in the confirm button
    sleep  2s

I fill the old password
    [Arguments]  ${old-password}
    input text    id=com.abaenglish.videoclass:id/oldPasswordEditTextInput    ${old-password}

I fill the first new password
    [Arguments]  ${new-password}
    input text    id=com.abaenglish.videoclass:id/newPasswordEditTextInput    ${new-password}

I fill the second new password
    [Arguments]  ${new-password}
    input text    id=com.abaenglish.videoclass:id/repeatNewPasswordEditTexInput   ${new-password}

I click in the confirm button
    tap     id=com.abaenglish.videoclass:id/changePasswordButton

The view prices button should appear in My account
    wait until page contains element    id=com.abaenglish.videoclass:id/premiumButton
    element should be enabled           id=com.abaenglish.videoclass:id/premiumButton

I should not see the view prices button in My Account
    wait until page contains element  id=com.abaenglish.videoclass:id/changePassword
    ${passed} =	    Run Keyword And Return Status	element should be enabled   id=com.abaenglish.videoclass:id/premiumButton
    Should Not Be True  ${passed} == True

I click in the change password button
    wait until page contains element  id=com.abaenglish.videoclass:id/changePassword
    tap  id=com.abaenglish.videoclass:id/changePassword

I click in the change premium offer button
    wait until page contains element    id=com.abaenglish.videoclass:id/premiumButton
    tap  id=com.abaenglish.videoclass:id/premiumButton

I scroll to the log-out button
    : FOR    ${i}    IN RANGE    0    10
    \    Swipe    15    600    15    100
    \    ${value}    Run Keyword And Return Status    Wait Until Page Contains Element    id=com.abaenglish.videoclass:id/logOutButton
    \    Run Keyword If    ${value}     Exit For Loop
    \    ${i}    Set Variable    ${i}+1


####--- LEVELS ---####

The user is on the main level page
    The element should be visible     id=com.abaenglish.videoclass:id/top_text

User should be inside of Menu levels
    wait until page contains element    xpath=//*[android.widget.FrameLayout]//*[contains(@text, 'Beginners')]

I am on the main level page
    The element should be visible     id=com.abaenglish.videoclass:id/top_text

I change of level
    [Arguments] 	${WISH_LEVEL}
    I go to Levels area using the menu
    I select a level    ${WISH_LEVEL}
    Sleep   3s

I select a level
    [Arguments] 	${SELECT_LEVEL}
    User should be inside of Menu levels
    tap  xpath=//*[android.widget.FrameLayout]//*[contains(@text, '${SELECT_LEVEL}')]
    sleep   1s
    #the section should have the text tittle    ${SELECT_LEVEL}   # no siempre ciincide por upper case de  momento hago la verificacion de qeu estoy dentro aparte
    ## falta hacer el swipe en caso de que no este la unidad en la pagina

I select the unit of the level
    [Arguments]     ${level-name}   ${unit-number}
    I select the level      ${level-name}
    I tap on a unit         ${unit-number}

I select the level
    [Arguments]     ${level-name}
    I go to Levels area using the menu
    click text      ${level-name}

I go to the begginers level
    I go to Levels area using the menu
    I select the begginers level

I select the begginers level
    wait until page contains element  xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[6]
    tap     xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[6]
    sleep  3s

#- These keywords are for the levels page after registration
I should be in the select level after registration
    [Documentation]      The user only access to this page after an account is created.
    [Arguments]     ${text}
    wait until page contains element    id=com.abaenglish.videoclass:id/top_text
    element should contain text         id=com.abaenglish.videoclass:id/top_text    ${text}

I select the level after registration
    [Arguments]     ${level-name}
    click text      ${level-name}


####--- YOUR CERTIFICATESS ---####
User should be redirected to your certificates section
    wait until page contains element    id=com.abaenglish.videoclass:id/main_fragment
    element should contain text    id=com.abaenglish.videoclass:id/toolbarSubTitle    YOUR CERTIFICATES


####--- CONTACT AND HELP PAGES PAGES ---####

I should be in the main page of the contact and help section
    The element should be visible   id=com.abaenglish.videoclass:id/zendesk_knowledge
    The element should be visible   id=com.abaenglish.videoclass:id/zendesk_rate

I go to the Rate the app section
    wait until page contains element  id=com.abaenglish.videoclass:id/zendesk_rate
    tap    id=com.abaenglish.videoclass:id/zendesk_rate

I go to the Help center section
    wait until page contains element  id=com.abaenglish.videoclass:id/zendesk_rate
    tap    id=com.abaenglish.videoclass:id/zendesk_knowledge

I go bock to the contact and help page
    I am on the Help center page
    I click in the back button in the help center page

I am on the Help center page
    The element should be visible   xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.view.View[1]/android.view.View[2]

I click in the back button in the help center page
    wait until page contains element  id=com.abaenglish.videoclass:id/toolbarLeftButton
    tap    id=com.abaenglish.videoclass:id/toolbarLeftButton

# These keywords are for the Googe Play Store page
I am on the Google Play story app
    The element should be visible   id=com.android.vending:id/action_bar_container_container
    The element should be visible   id=com.android.vending:id/uninstall_button
    The element should be visible   id=com.android.vending:id/launch_button


####--- COURSE PAGE ---####
#-Unit page
#I tap on a unit
#    [Arguments]  ${unit}
#    #wait until page contains element    id=com.abaenglish.videoclass:id/main_fragment
##    Swipe UP    xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${unit}')]
#    Swipe DOWN    xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${unit}')]
#    wait until page contains element    xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${unit}')]
#    tap    xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${unit}')]

I tap on a unit
    [Arguments]  ${unit}
    wait until page contains element    id=com.abaenglish.videoclass:id/main_fragment
    Swipe UP    xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${unit}')]
    #wait until page contains element    xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${unit}')]
    tap    xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${unit}')]

Unit should be shown
    [Arguments]  ${unit}
    [TearDown]    Run Keyword If Test Failed     Swipe UP   xpath=//*[android.widget.LinearLayout]//*[contains(@text, '6')]
     Wait Until Page Contains Element      xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${unit}')]

User Should be inside of unit
    [Arguments]  ${unit}
     wait until page contains element   xpath=//android.widget.LinearLayout/..//*[contains(@text, '${unit}')]

the section should have the text tittle
     [Arguments]  ${TITTLE}
     wait until page contains element     xpath=//android.widget.LinearLayout/..//*[contains(@text, '${TITTLE}')]

#-Course page
The user is on the main course page
    The element should be visible     id=com.abaenglish.videoclass:id/toolbarTitle

I tap in the back button in the course page
    wait until page contains element    id=com.abaenglish.videoclass:id/toolbarLeftButton
    tap    id=com.abaenglish.videoclass:id/toolbarLeftButton

I go to the first unit of beginners
    I go to the begginers level
    I click in the first unit of beginners level

I click in the first unit of beginners level
    sleep  2s
    wait until page contains element    xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ExpandableListView[1]/android.widget.LinearLayout[5]
    I scroll to the text    ${first unit - tittle}
    click text  ${first unit - tittle}

#- The following two should be deleted.
I go to the second unit of beginners
    I go to the begginers level
    I click in the second unit of beginners level

I click in the second unit of beginners level
    sleep  2s
    wait until page contains element    xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ExpandableListView[1]/android.widget.LinearLayout[5]
    I scroll to the text    ${second unit - tittle}
    click text  ${second unit - tittle}

I click in the download unit button
    wait until page contains element    id=com.abaenglish.videoclass:id/toolbarRightButton
    tap     id=com.abaenglish.videoclass:id/toolbarRightButton

The progress of the unit is
    [Arguments]     ${progress}
    wait until page contains element    id=com.abaenglish.videoclass:id/toolbarSubTitle
    element should contain text     id=com.abaenglish.videoclass:id/toolbarSubTitle     ${progress}


##- These keyworks are to the selection the class
#I tap on section
#    [Arguments]  ${SECTION}
#    wait until page contains element    id=com.abaenglish.videoclass:id/sections_list
#    ${value}    Run Keyword And Return Status    The element should be visible    ${text}
#    Run Keyword If	${value} == False    Swipe DOWN      xpath=//*[android.widget.RelativeLayout]//*[contains(@text, '${SECTION}')]
##    Swipe DOWN      xpath=//*[android.widget.RelativeLayout]//*[contains(@text, '${SECTION}')]
##    wait until page contains element    xpath=//*[android.widget.RelativeLayout]//*[contains(@text, '${SECTION}')]
#    tap    xpath=//*[android.widget.RelativeLayout]//*[contains(@text, '${SECTION}')]


I tap on section
    [Arguments]  ${SECTION}
    wait until page contains element    id=com.abaenglish.videoclass:id/sections_list
    wait until page contains element    xpath=//*[android.widget.RelativeLayout]//*[contains(@text, '${SECTION}')]
    tap    xpath=//*[android.widget.RelativeLayout]//*[contains(@text, '${SECTION}')]

I select the aba film class
    wait until page contains element    ${sel-unit page aba film class - lista}
    Tap    ${sel-unit page aba film class - lista}

I select the speaking class
    wait until page contains element    ${sel-unit page speaking class - lista}
    Tap     ${sel-unit page speaking class - lista}

I select the write class
    Tap    ${sel-unit page write class - lista}

I select the interpret class
   Tap     ${sel-unit page interpret class - lista}

I select the video class
    Tap    ${sel-unit page videoclass class - lista}

I select the exercises class
    Tap     ${sel-unit page exercises class - lista}

I select the vocabulary class
    Tap    ${sel-unit page vocabulary class - lista}

I select the assessment
    wait until page contains element    ${sel-unit page aba film class - lista}
    I scroll to the element     ${sel-unit page assessment - lista}
    Tap    ${sel-unit page assessment - lista}


## ----- These keywords are for the ABA film and VideoClas -----------
I complete the aba film class
    I select the aba film class
    I start the aba film class
    I advance the video    10      13
    sleep  5s

I complete the videoclass class
    [Documentation]     Top 3 lines are for the A/B Test of Lineal experience, these can be deleted after the test and change with the new process.
    ...     True: When the user has the lineal experience. False: The user is on the unit page (lista-rosco).
    sleep  5s
    ${text}     get text    id=com.abaenglish.videoclass:id/toolbarTitle
    ${passed} =	    Run Keyword And Return Status	element should contain text     id=com.abaenglish.videoclass:id/toolbarTitle   Video Class
    Run Keyword If	${passed} == False    I select the video class
    Run Keyword If	${passed} == False    I start the aba film class
    Run Keyword If	${passed} == True     I start the aba film class
    I advance the video    20      61
    sleep  5s

I start the aba film class
    wait until page contains element    id=com.abaenglish.videoclass:id/play_button
    tap  id=com.abaenglish.videoclass:id/play_button

I start the ABA film with subtitles activated
    I select the aba film class
    I see the video with subtitles
    I start the aba film class

I start the ABA film with subtitles desactivated
    I select the aba film class
    I see the video without subtitles
    I start the aba film class

I advance the video
    [Documentation]     First try to click as much as possible the forward button, then the keyword waits until the header appears to continue the test.
    ...     The first variable (click-times) is the amount of clicks to adelantar the video, the second (wait-times) is the amount of times the keywords waits that the header appears
    [Arguments]  ${click-times}     ${wait-times}
    : FOR    ${i}    IN RANGE    0    ${click-times}
    \   Run Keyword And Ignore Error    I click in the forward video
    I wait until the video is completed     ${wait-times}

I click in the forward video
    I click if the element is visible    id=android:id/ffwd

I wait until the video is completed
    [Documentation]     Should be checked
    [Arguments]     ${wait-times}
    : FOR    ${i}    IN RANGE    0    ${wait-times}
    \   ${value}    Run Keyword And Return Status   The element should be visible     id=com.abaenglish.videoclass:id/toolbarTitle
    \   Run keyword if    ${value} == True      Exit For Loop
    \   Run keyword if    ${value} == False     sleep  5s

I click in the advance button of the video
    ${passed} =	    Run Keyword And Return Status	The element should be visible       id=android:id/ffwd
    Run Keyword If	${passed} == False   click a point   100      100
    Run Keyword If	${passed} == True    I click if the element is visible    id=android:id/ffwd

I see the video with subtitles
    wait until page contains element  id=com.abaenglish.videoclass:id/downLayout
    tap     id=com.abaenglish.videoclass:id/downLayout
    tap     id=com.abaenglish.videoclass:id/subThreeText

I see the video without subtitles
    wait until page contains element  id=com.abaenglish.videoclass:id/downLayout
    tap     id=com.abaenglish.videoclass:id/downLayout
    tap     id=com.abaenglish.videoclass:id/subSecondText

I should be able to see the subtitles in the ABA Film
    sleep  2s
    wait until page contains    very nice here
    wait until page contains    Yes, the weather is wonderful.

I should not be able to see the subtitles in the ABA Film
    sleep  2s
    ${passed} =     Run Keyword And Return Status   wait until page contains    very nice here
    Should Not Be True  ${passed} == True


## ------ These keywords are for the speaking class
I complete the speak class
    [Documentation]     Top 3 lines are for the A/B Test of Lineal experience, these can be deleted after the test and change with the new process.
    ...         True: When the user has the lineal experience. False: The user is on the unit page (lista-rosco).
    sleep  5s
    ${text}     get text    id=com.abaenglish.videoclass:id/toolbarTitle
    ${passed} =	    Run Keyword And Return Status	element should contain text     id=com.abaenglish.videoclass:id/toolbarTitle   Speak
    Run Keyword If	${passed} == False    I select the speaking class
    Run Keyword If	${passed} == False    I start the speak class
    Run Keyword If	${passed} == True     I start the speak class
#    I wait until the element does not contain the text  Speak   200     15
    I wait until the element does not contain the text  Speak   250     2

I start the speak class
    wait until page contains element  id=com.abaenglish.videoclass:id/centerButtonView
    tap     id=com.abaenglish.videoclass:id/centerButtonView
    I accept the access in the pop-up of the unit page
    tap     id=com.abaenglish.videoclass:id/centerButtonView

I start the speak class without accept the pop-up
    wait until page contains element  id=com.abaenglish.videoclass:id/centerButtonView
    tap     id=com.abaenglish.videoclass:id/centerButtonView

## ----- These keywords are for the write class
I complete the write class
    [Documentation]     Top 3 lines are for the A/B Test of Lineal experience, these can be deleted after the test and change with the new process.
    ...     True: When the user has the lineal experience. False: The user is on the unit page (lista-rosco).
    sleep  5s
    ${text}     get text    id=com.abaenglish.videoclass:id/toolbarTitle
    ${passed} =	    Run Keyword And Return Status	element should contain text     id=com.abaenglish.videoclass:id/toolbarTitle   Write
    Run Keyword If	${passed} == False    I select the write class
    Run Keyword If	${passed} == False    I start the write class
    Run Keyword If	${passed} == True     I start the write class
#    I wait until the element does not contain the text  Write   200     10
    I wait until the element does not contain the text  Write   200     2

I start the write class
    wait until page contains element    id=com.abaenglish.videoclass:id/listen_button
    tap  id=com.abaenglish.videoclass:id/listen_button


## ------ These keywords are for the interpret class
I complete the interpret class
    [Documentation]     Top 3 lines are for the A/B Test of Lineal experience, these can be deleted after the test and change with the new process.
    ...     True: When the user has the lineal experience. False: The user is on the unit page (lista-rosco).
    sleep  5s
    ${text}     get text    id=com.abaenglish.videoclass:id/toolbarTitle
    ${passed} =	    Run Keyword And Return Status	element should contain text     id=com.abaenglish.videoclass:id/toolbarTitle   Interpret
    Run Keyword If	${passed} == False    I select the interpret class
    Run Keyword If	${passed} == False    I complete the first interpreter
    Run Keyword If	${passed} == True     I complete the first interpreter
    I complete the second interpreter

I complete the first interpreter
    I click in the first interpreter
    I start the interpret video
    I wait until the page does not contain the element  id=com.abaenglish.videoclass:id/centerButton     15

I complete the second interpreter
    I click in the second interpreter
    I start the interpret video
    I wait until the page does not contain the element  id=com.abaenglish.videoclass:id/centerButton     15

I click in the first interpreter
    wait until page contains element  xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.ListView[1]/android.widget.LinearLayout[1]
    tap  xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.ListView[1]/android.widget.LinearLayout[1]

I click in the second interpreter
    wait until page contains element  xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.ListView[1]/android.widget.LinearLayout[2]
    tap  xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.ListView[1]/android.widget.LinearLayout[2]

I start the interpret video
    wait until page contains element  id=com.abaenglish.videoclass:id/centerButton
    tap  id=com.abaenglish.videoclass:id/centerButton


## ----- These keywords are for the exercises class
I complete the execirses class
    [Documentation]     Top 3 lines are for the A/B Test of Lineal experience, these can be deleted after the test and change with the new process.
    ...                 True: When the user has the lineal experience. False: The user is on the unit page (lista-rosco).
    sleep  5s
    wait until page contains element    id=com.abaenglish.videoclass:id/toolbarTitle
    ${passed} =	    Run Keyword And Return Status	element should contain text     id=com.abaenglish.videoclass:id/toolbarTitle   Exercises
    Run Keyword If	${passed} == False    I select the exercises class
    Run Keyword If	${passed} == False    I start the exercies class
    Run Keyword If	${passed} == True     I start the exercies class
    I wait until the element does not contain the text  Exercises   80     2

I start the exercies class
    wait until page contains element    id=com.abaenglish.videoclass:id/checkAnswersButton
    tap  id=com.abaenglish.videoclass:id/checkAnswersButton


## ------- These keywords are for the vocabulary class
I complete the vocabulary class
    [Documentation]     Top 3 lines are for the A/B Test of Lineal experience, these can be deleted after the test and change with the new process.
#    [Documentation]     True: When the user has the lineal experience. False: The user is on the unit page (lista-rosco).
    sleep  5s
    wait until page contains element    id=com.abaenglish.videoclass:id/toolbarTitle
    ${passed} =	    Run Keyword And Return Status	element should contain text     id=com.abaenglish.videoclass:id/toolbarTitle   Vocabulary
    Run Keyword If	${passed} == False    I select the vocabulary class
    Run Keyword If	${passed} == False    I start the vocabulary class
    Run Keyword If	${passed} == True     I start the vocabulary class
#    I wait until the element does not contain the text  Vocabulary   150     10
    I wait until the element does not contain the text  Vocabulary   200     2

I start the vocabulary class
    wait until page contains element    id=com.abaenglish.videoclass:id/centerButton
    tap  id=com.abaenglish.videoclass:id/centerButton


## ----- These keywords are for the assessment
I complete the assessment
    [Documentation]     Top 3 lines are for the A/B Test of Lineal experience, these can be deleted after the test and change with the new process.
    sleep  5s
    wait until page contains element    id=com.abaenglish.videoclass:id/toolbarTitle
    Run Keyword And Return Status   Hide Keyboard
    ${passed} =	    Run Keyword And Return Status	element should contain text     id=com.abaenglish.videoclass:id/toolbarTitle   Assessment
    Run Keyword If	${passed} == False    I select the assessment

I complete the assessment of the first unit without errors
    I complete the assessment
    I start the asessment of the unit   1
    I complete questions of the assessment      correct    10

I start the asessment of the unit
    [Arguments]     ${unit-number}
    ${unit}   Convert To String   ${unit-number}
    ${status} =	    Run Keyword And Return Status    Should Be Equal  ${unit}  1
    Run Keyword If	${status} == True       Set Global Variable	    ${assessment-file}	   ${assessment-file-answers-unit-1}
    I click in the start assessment button

I complete questions of the assessment
    [Arguments]     ${type-question}     ${amount-questions}
    @{listElements} =   I read information of the file  ${assessment-file}  0
    ${status} =	    Run Keyword And Return Status    Should Be Equal  ${type-question}  correct
    Run Keyword If	${status} == True       Set Suite Variable	${file-column}	   1
    Run Keyword If	${status} == False      Set Suite Variable	${file-column}	   2
    log     ${file-column}
    repeat keyword      ${amount-questions} times     I select the answer in the assessment     ${file-column}  @{listElements}

I select the answer in the assessment
    [Arguments]     ${column-number}    @{listElements}
    ${question-number-string}   Convert To String   ${question-number}
    ${ANSWER-ID} =      I save the value of a list with split values and values comparison  ${question-number-string}  0   ${column-number}   :  @{listElements}
    sleep   1s
    click text     ${ANSWER-ID}
    ${contador} =  Convert to integer  ${question-number}
    ${contador} =  evaluate  ${contador} + 1
    set global variable     ${question-number}      ${contador}

I should see the assessment result
    [Arguments]     ${results}
    wait until page contains element    id=com.abaenglish.videoclass:id/correctExercice
    element should contain text         id=com.abaenglish.videoclass:id/correctExercice     ${results}

I can start the test again
    I click in the repeat the test button
    The user is on the first question

I click in the start assessment button
    wait until page contains element    id=com.abaenglish.videoclass:id/start_evaluation
    tap     id=com.abaenglish.videoclass:id/start_evaluation
    set global variable     ${question-number}      1

Confirm the user complete the unit one
    wait until page contains element  id=com.abaenglish.videoclass:id/continue_nextUnit
    page should contain text   10 from 10
    element should contain text  id=com.abaenglish.videoclass:id/continue_nextUnit    carry on with the unit 2

I go to the next unit after assessment
    wait until page contains element  id=com.abaenglish.videoclass:id/continue_nextUnit
    tap     id=com.abaenglish.videoclass:id/continue_nextUnit

I check the wrong answers
    wait until page contains element  id=com.abaenglish.videoclass:id/repeat_evaluation
    tap     id=com.abaenglish.videoclass:id/repeat_evaluation

I can see the wrong answer
    wait until page contains element  id=com.abaenglish.videoclass:id/evaluationOption1
    element should contain text     id=com.abaenglish.videoclass:id/evaluationNumber     Question 2 from 10
    page should contain text  He am at the beach

I can complete the valid answer
    wait until page contains element  id=com.abaenglish.videoclass:id/evaluationOption1
    click text  is
    Confirm the user complete the unit one

I lost the assessment
    wait until page contains element  id=com.abaenglish.videoclass:id/repeat_evaluation
    page should contain text  Try again!
    page should contain text  To pass the test, you must get at least 8/10

I click in the repeat the test button
    wait until page contains element  id=com.abaenglish.videoclass:id/repeat_evaluation
    tap     id=com.abaenglish.videoclass:id/repeat_evaluation

The user is on the first question
    wait until page contains element  id=com.abaenglish.videoclass:id/evaluationNumber
    element should contain text     id=com.abaenglish.videoclass:id/evaluationNumber     Question 1 from 10


I am in the unit
    [Arguments]  ${unit}    ${unit-tittle}
    wait until page contains element  id=com.abaenglish.videoclass:id/toolbarTitle
    element should contain text  id=com.abaenglish.videoclass:id/toolbarTitle      ${unit}
    element should contain text  id=com.abaenglish.videoclass:id/toolbarSubTitle   ${unit-tittle}


## ---- These keywrods are for the permissions pop-up, text box and button.
I accept the access in the pop-up of the unit page
    repeat keyword      4 times     I click in the ok button in the pop-up

I click in the ok button in the pop-up
    ${passed} =	    Run Keyword And Return Status	element should be enabled       id=com.android.packageinstaller:id/permission_allow_button
    Run Keyword If	${passed} == True       tap    id=com.android.packageinstaller:id/permission_allow_button

I decline the access in the pop-up in the unit page
    tap     id=com.android.packageinstaller:id/permission_deny_button

The permissions button redirects to the phone settings page
    I click in the settings button
    The user is in the settings page

I click in the settings button
    tap     id=com.abaenglish.videoclass:id/snackbar_action

The user is in the settings page
    The element should be visible         id=com.android.settings:id/all_details


####--- PLANS PAGE ---####
I should see at least two prooducts in the plan page
    wait until page contains element    ${sel-payment one-month plan}
    element should be enabled   ${sel-payment one-month plan}
    element should be enabled   ${sel-payment six-months plan}

#### new pan page
I tap on view prices from my account
    element should be enabled   xpath=//android.widget.Button/..//*[contains(@text, 'View prices')]
    Tap    xpath=//android.widget.Button/..//*[contains(@text, 'View prices')]
    new plan page should be displayed

I tap on see prices button
     element should be enabled   id=com.abaenglish.videoclass:id/premiumButton
     Tap    id=com.abaenglish.videoclass:id/premiumButton
     new plan page should be displayed

new plan page should be displayed
    wait until page contains element    id=com.abaenglish.videoclass:id/main_fragment
    element should contain text   id=com.abaenglish.videoclass:id/textViewGoPremium    Go Premium!

Choose an ABA Premium subscription tittle should be present
    element should contain text    id=com.abaenglish.videoclass:id/toolbarTitle    Choose an ABA Premium subscription

list prices should be present
     element should contain text   id=com.abaenglish.videoclass:id/textViewPlansAndPrices   Plans & prices
     wait until page contains element   id=com.abaenglish.videoclass:id/textViewPlansAndPrices
     wait until page contains element   id=com.abaenglish.videoclass:id/linearLayoutPlans
     wait until page contains element   id=com.abaenglish.videoclass:id/itemPlanButton

Plan page should contain a rate plan by
    [Arguments] 	${RATEPLAN}
    element should be enabled   id=com.abaenglish.videoclass:id/itemPlanMainLayout
    wait until page contains element    xpath=//*[android.widget.LinearLayout]//*[contains(@text, '${RATEPLAN}')]

Monthly plan should be present
    element should be enabled   id=com.abaenglish.videoclass:id/itemPlanMainLayout
    wait until page contains element  xpath=//*[android.widget.LinearLayout]//*[contains(@text, 'MONTHLY')]

SixMonthly plan should be present
    element should be enabled   id=com.abaenglish.videoclass:id/itemPlanMainLayout
    wait until page contains element  xpath=//*[android.widget.LinearLayout]//*[contains(@text, 'SIX-MONTHLY')]

Annual plan should be present
    element should be enabled   id=com.abaenglish.videoclass:id/itemPlanMainLayout
    wait until page contains element     xpath=//*[android.widget.LinearLayout]//*[contains(@text, 'YEARLY')]

Recurring billing cancel anytime message should be present
    element should contain text    id=com.abaenglish.videoclass:id/textViewRecurringBilling    Recurring billing

I tap on the monthly plan button
    tap    xpath: //android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.ImageView[1]

I tap on the Six-monthly plan button
    tap   xpath: //android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[2]/android.widget.LinearLayout[1]/android.widget.ImageView[1]

I tap on the annual plan button
    tap   xpath: //android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[3]/android.widget.LinearLayout[1]/android.widget.ImageView[1]


################goldbuster 111 teacher layout

then improved message of a teacher should be shown
    wait until page contains element    id=com.abaenglish.videoclass:id/teacherImageView

the popup should have a message about Carry on studying with the section Speak
  element should contain text   id=com.abaenglish.videoclass:id/finishedSectionTextView    Carry on studying with the section Speak

continue button should be shown in popup
    wait until page contains element    id=com.abaenglish.videoclass:id/buttonNextSection

exit button should be present
    wait until page contains element    id=com.abaenglish.videoclass:id/quitButton

I tap on continue button
    tap    id=com.abaenglish.videoclass:id/buttonNextSection


#### ??
I am in the white Premium page
    wait until page contains element    id=com.abaenglish.videoclass:id/button_view_prices
    element should contain text  id=com.abaenglish.videoclass:id/textview_title  The content you wish to access is exclusively for students with ABA Premium

I go to the Plan page from the premium white page
    I click in the view prices button in the white premium page

I click in the view prices button in the white premium page
    wait until page contains element    id=com.abaenglish.videoclass:id/button_view_prices
    tap     id=com.abaenglish.videoclass:id/button_view_prices


### lAYOUT TO CONVINCE THE USER TO BE PREMIUM
Layout of exclusively for students with ABA premium should be displayed
    wait until page contains element    xpath=//*[android.widget.ScrollView]//*[contains(@text, 'exclusively')]
    wait until page contains element    xpath=//*[android.widget.LinearLayout]//*[contains(@text, 'prices')]

I tap on View prices
    tap    xpath=//*[android.widget.LinearLayout]//*[contains(@text, 'prices')]


### ----- ABA MOMENTS FEATURE
#- ABA Moments menu
I start the colors activity
   wait until page contains element    id=com.abaenglish.videoclass:id/abaMomentsTextView
   I scroll to the element      xpath=//android.widget.RelativeLayout[@content-desc='adf6bbbb-104f-4911-b648-66e7e61c3879']
   tap    xpath=//android.widget.RelativeLayout[@content-desc='adf6bbbb-104f-4911-b648-66e7e61c3879']
   sleep    5s

I start the animals activity
   wait until page contains element    id=com.abaenglish.videoclass:id/abaMomentsTextView
   I scroll to the element      xpath=//android.widget.RelativeLayout[@content-desc='42c389e0-b2b7-4a47-8321-93a1a2ac38e8']
   tap    xpath=//android.widget.RelativeLayout[@content-desc='42c389e0-b2b7-4a47-8321-93a1a2ac38e8']
   sleep    5s

I should be in the ABA Moment menu
    The element should be visible   xpath=//android.widget.RelativeLayout[@content-desc='adf6bbbb-104f-4911-b648-66e7e61c3879']

I should see in the aba moments description
    [Arguments]     ${text}
    wait until page contains element    id=com.abaenglish.videoclass:id/abaMomentsTextView
    Element Should Contain Text         id=com.abaenglish.videoclass:id/abaMomentsTextView   ${text}

I should see the colors activity with the name
    [Arguments]     ${name}
    wait until page contains element     xpath=//android.widget.RelativeLayout[@content-desc='adf6bbbb-104f-4911-b648-66e7e61c3879']
    ${value}   Get Text    xpath=//android.widget.RelativeLayout[@content-desc='adf6bbbb-104f-4911-b648-66e7e61c3879']/android.widget.TextView[1][@resource-id='com.abaenglish.videoclass:id/ABAMomentTitle']
#    Should be equal     ${value}    ${name}
    element should contain text     xpath=//android.widget.RelativeLayout[@content-desc='adf6bbbb-104f-4911-b648-66e7e61c3879']     ${name}

I should see the animals activity with the name
    [Arguments]     ${name}
    wait until page contains element    id=com.abaenglish.videoclass:id/abaMomentsTextView
    I scroll to the element     xpath=//android.widget.RelativeLayout[@content-desc='42c389e0-b2b7-4a47-8321-93a1a2ac38e8']
    ${value}   Get Text    xpath=//android.widget.RelativeLayout[@content-desc='42c389e0-b2b7-4a47-8321-93a1a2ac38e8']/android.widget.TextView[1][@resource-id='com.abaenglish.videoclass:id/ABAMomentTitle']
    Should be equal     ${value}    ${name}


#- ABA Moments activities
I complete this amount of aba activities
    [Arguments]     ${amount-activities}
    @{listElements} =   I read information of the file  ${abamoments-file-answers}  0
    repeat keyword      ${amount-activities} times     I complete the aba activity     @{listElements}

I select a wrong answer
    @{listElements} =   I read information of the file  ${abamoments-file-answers}  0
    ${QUESTION-ID} =    I save the question ID
#    ${ANSWER-ID} =      Save the first wrong answer of the question     ${QUESTION-ID}  @{listElements}
    ${ANSWER-ID} =      I save the value of a list with split values and values comparison  ${QUESTION-ID}  0   2   :  @{listElements}
    I select the answer     ${ANSWER-ID}

I save the question ID
    wait until page contains element    id=com.abaenglish.videoclass:id/questionCircleABAMomentView
    ${value} =  Get Element Attribute   id=com.abaenglish.videoclass:id/questionCircleABAMomentView    name
    [Return]    ${value}

I select the answer
    [Arguments]  ${answer}
    ${selector} =	Catenate	SEPARATOR=      xpath=//android.view.View[@content-desc='     ${answer}     ']
    tap  ${selector}
    sleep   2s

I complete the aba activity
    [Arguments]     @{listElements}
    Set Suite Variable	${column-returned}    1
    ${QUESTION-ID} =    I save the question ID
    ${ANSWER-ID} =      I save the value of a list with split values and values comparison  ${QUESTION-ID}  0   1   :  @{listElements}
    sleep   1s
    I select the answer     ${ANSWER-ID}

I close the activity
    wait until page contains element    id=com.abaenglish.videoclass:id/quitButton
    tap                                 id=com.abaenglish.videoclass:id/quitButton

I should be in the question
    [Arguments]  ${question}
    wait until page contains element    id=com.abaenglish.videoclass:id/quitButton
    ${value} =  Get Element Attribute   id=com.abaenglish.videoclass:id/progressBar    name
    Should Be Equal     ${question}     ${value}

I accept the ABA Moment confirmation pop-up
    sleep   2s
    wait until page contains element    id=com.abaenglish.videoclass:id/nextUnitButton
    tap                                 id=com.abaenglish.videoclass:id/nextUnitButton

I close the ABA Moment confirmation pop-up
    sleep   2s
    wait until page contains element    id=com.abaenglish.videoclass:id/quitButton
    tap                                 id=com.abaenglish.videoclass:id/quitButton

I wait until ABA Moment confirmation pop-up does not exist
    wait until page contains element            id=com.abaenglish.videoclass:id/nextUnitButton
    I wait until the page does not contain the element  id=com.abaenglish.videoclass:id/nextUnitButton     5


######  PENGING KEYWORDS

user should be on beginners level

user should be on lower intermediate level

user should be on intermediate level

user should be on upper intermediate level

user should be on advanced level

user should be on business level

user should be inside of level
     [Arguments]  ${LEVEL}

## options blocked in units

Aba Film option in the unit should be blocked

Speak option in the unit should be blocked

Write option in the unit should be blocked

Interpret option in the unit should be blocked

Exercises option in the unit should be blocked

Vocabulary option in the unit should be blocked

Assesment option in the unit should be blocked


## optiosn available in units
Aba Film option in the unit should be available

Speack option in the unit should be available

Write option in the unit should be available

Interpret option in the unit should be available

Exercises option in the unit should be available

Vocabulary option in the unit should be available

Assesment option in the unit should be available

Video Class option in the unit should be available


######  units
I tap on ABA FILM option

I tap on Speak option

I tap on Write option

I tap on interpret option

I tap on Exercises option

I tap on Video Class option

I tap on Vocabulary option

I tap on Assessmet option


#####  Message to Be premium User

Why make you premium message should be displayed

tap on start tour premium course

tap on back button

#####Plan Plage

new plan page sholh be displayed

old plan page should be displayed