*** Settings ***
Documentation  This file contains the tests of the course and avergae progress in the Android Application
Resource    ../config.robot
#Resource    ../../../startDevice.robot
Library  AppiumLibrary
Library  String

#Suite Set Up        I start the ABA application
Test Set Up         I start the ABA application
Test Tear Down      close application
#                        Run Keywords
#...				    Run Keyword If Test Failed    close application   AND
#...                 Run Keyword If Test Failed	get screen shot
#Suite Tear Down	    Reset application

*** Test Cases ***
Free users are redirected to the white premium page when they click in the ABA Film of the second unit, then they can go to the plan page
    [Tags]  android     medium    real
    Given I login in the application with free user
    And I go to the second unit of beginners
    When I select the aba film class
    Then I am in the white Premium page
    And I go to the Plan page from the premium white page
    And I should see at least two prooducts in the plan page