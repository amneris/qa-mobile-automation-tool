*** Settings ***
Documentation  This file contains tests of the course in the Android Application
Resource    ../../config.robot
Library  AppiumLibrary
Library  String

Test Set Up         I start the ABA application
Test Tear Down      close application


*** Test Cases ***
User should be able to see the progress after the aba film is completed
    [Tags]  android     critical    real
    Given I create an user and I go to the level     Beginners
    And I complete the aba film class
    And reset application
    And I login in the application  ${RANDOM USER-EMAIL}  ${FREE USER-PASSWORD}
    And I select the level  Beginners
    And I click in the first unit of beginners level
    Then The progress of the unit is  12%

The aba film should display the subtitles when the option is actived
    [Tags]  android     medium    real
    Given I login in the application with premium user
    When I select the level  Beginners
    And I click in the first unit of beginners level
    And I start the ABA film with subtitles activated
    Then I should be able to see the subtitles in the ABA Film

The aba film should not display the subtitles when the option is desactived
    [Tags]  android     medium    real
    Given I login in the application with premium user
    When I select the level  Beginners
    And I click in the first unit of beginners level
    And I start the ABA film with subtitles desactivated
    Then I should not be able to see the subtitles in the ABA Film

User should be able to complete first Unit of beginners level
    [Tags]  android     medium    real      completeUnit    test1
    Given I create an user and I go to the level     Beginners
    When I complete the aba film class
    And I complete the speak class
    And I complete the write class
    And I complete the interpret class
    And I complete the videoclass class
    And I complete the execirses class
    And I complete the vocabulary class
    And I complete the assessment of the first unit without errors
    And I go to the next unit after assessment
    Then I am in the unit   Unit 2      A New Friend