*** Settings ***
Documentation  This file contains tests of the assessment in the Android Application
Resource    ../../config.robot
Library  AppiumLibrary
Library  String

Test Set Up         I start the ABA application
Test Tear Down      close application


*** Test Cases ***
I should be able to complete the assessment of the first unit and be redirected to the second unit
    [Tags]  android     medium    real  assessment
    Given I login in the application with premium user
    When I select the level  Beginners
    And I click in the first unit of beginners level
    And I select the assessment
    And I start the asessment of the unit   1
    And I complete questions of the assessment      correct    10
    And I should see the assessment result          10 from 10
    And I go to the next unit after assessment
    Then I am in the unit   Unit 2      A New Friend

I should be able to complete the assessment of the first unit and see the answer that had an error
    [Tags]  android     medium    real  assessment
    Given I login in the application with premium user
    And I select the level  Beginners
    And I click in the first unit of beginners level
    And I select the assessment
    And I start the asessment of the unit   1
    When I complete questions of the assessment   correct    1
    And I complete questions of the assessment    invalid    1
    And I complete questions of the assessment    correct    8
    And I should see the assessment result     9 from 10
    And I check the wrong answers
    Then I can see the wrong answer
    And I can complete the valid answer

User should lost the assessment with only 4 correct answers
    [Tags]  android     medium    real  assessment
    Given I login in the application with premium user
    And I select the level  Beginners
    And I click in the first unit of beginners level
    And I select the assessment
    And I start the asessment of the unit   1
    When I complete questions of the assessment   correct    3
    And I complete questions of the assessment    invalid    6
    And I complete questions of the assessment   correct    1
    Then I should see the assessment result     4 from 10
    And I lost the assessment
    And I can start the test again