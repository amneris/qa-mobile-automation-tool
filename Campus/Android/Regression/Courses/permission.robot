*** Settings ***
Documentation  This file contains tests of the assessment in the Android Application.
...     These test cases should be executed in real devices or emulators with api 21 or higher and deny permissions before starts the execution.

Library  AppiumLibrary

Resource    ../config.robot

Test Set Up         I start the ABA application
Test Tear Down      close application

*** Test Cases ***
I should see the permissions pop-up to download an unit and I could be redirected to the settings page
    [Tags]  android     main    real    permissions
    Given I login in the application with premium user
    When I select the level  Beginners
    And I click in the first unit of beginners level
    And I click in the download unit button
    When I decline the access in the pop-up in the unit page
    Then The permissions button redirects to the phone settings page

I should see the permissions pop-ups to start the speaking class and I could be redirected to the settings page
    [Tags]  android     main    real    permissions
    Given I login in the application with premium user
    When I select the level  Beginners
    And I click in the first unit of beginners level
    And I select the speaking class
    And I start the speak class without accept the pop-up
    And I decline the access in the pop-up in the unit page
    And I decline the access in the pop-up in the unit page
    Then The permissions button redirects to the phone settings page