*** Settings ***
Documentation   The file contains the test cases of the login pages: initial page, login page, create user page and forgot password page
Library  AppiumLibrary

Resource    ../../config.robot


#Suite Set Up        I start the ABA application
Test Set Up         I start the ABA application
Test Tear Down      close application
#                        Run Keywords
#...				    Run Keyword If Test Failed    close application   AND
#...                 Run Keyword If Test Failed	get screen shot
#Suite Tear Down	    Reset application


*** Test Cases ***
# ------------- Test cases are of the creation account functionality. ---------------------
User should be able to create an account using Android application
    [Tags]  android     critical    real    emulator    loginPages
    Given I go to the creation user page
    When I create an user with random email
    Then I should be in the select level after registration     You can change your course level after

User cannot create an account with an email that has been registered previously.
    [Tags]  android    medium   real    emulator    loginPages
    Given I go to the creation user page
    When I create an accont with the free user email
    Then The log-in pop-up redirects the user the to the Login Page


# ------------- Test cases are of the recover password functionality. ---------------------
User should be able to recover his password
    [Tags]  android     critical    real    emulator    loginPages
    Given I go to the recover password page
    When I recover the password for the email    ${FREE USER-EMAIL}
    Then The pop-up to recover the password has valid message
    And The pop-up to recover the password can be closed
