*** Settings ***
Documentation  This file contains the keywords used in the initial page when users start the application
Resource    ../config.robot
#Resource    ../../../startDevice.robot
Library  AppiumLibrary
Library  String

#Suite Set Up        I start the ABA application
Test Set Up         I start the ABA application
Test Tear Down      close application
#                        Run Keywords
#...				    Run Keyword If Test Failed    close application   AND
#...                 Run Keyword If Test Failed	get screen shot
#Suite Tear Down	    Reset application

*** Variables ***
${sel-main allow notifications button}      xpath=//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[6]/XCUIElementTypeOther[2]/XCUIElementTypeAlert[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeButton[1]
${sel-main login register now button}       xpath=//XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
${sel-main login already a user button}     xpath=//XCUIElementTypeOther[1]/XCUIElementTypeButton[2]


#################  pendiente por automatizar
*** Testcases ***

Beginners- old free users should see - Why make you premium - page when they try to access to a blocked section
    [tags]              GOLDBUSTER-9
    [Documentation]     GOLDBUSTER-9 Why make you premium pagehey try to access to blocked sections - beginners
    Given I login in the application  ${AA}   ${PASS}
    when I change of level   beginners
    then I tap on a unit   2
    then Speak option in the unit should be blocked
    then I tap on Speak option
    And Why make you premium message should be displayed
    then tap on start tour premium course
    And old plan page should be displayed

Lower intermediate- old free users should see - Why make you premium - page when they try to access to a blocked section
    [tags]              GOLDBUSTER-9
    [Documentation]     GOLDBUSTER-9 Why make you premium pagehey try to access to blocked sections - lower int
    Given I login in the application  ${AA}   ${PASS}
    When I change of level   lower intermediate
    then I tap on a unit   26
    then Write option in the unit should be blocked
    then I tap on Write option
    And Why make you premium message should be displayed
    then tap on start tour premium course
    And old plan page should be displayed


intermediate- old free users should see - Why make you premium - page when they try to access to a blocked section
    [tags]              GOLDBUSTER-9
    [Documentation]     GOLDBUSTER-9 Why make you premium pagehey try to access to blocked sections - intermediate
    Given I login in the application  ${AA}   ${PASS}
    When I change of level   intermediate
    then I tap on a unit   50
    then Exercises option in the unit should be blocked
    then I tap on Exercises option
    And Why make you premium message should be displayed
    then tap on start tour premium course
    And old plan page should be displayed

advanced- old free users should see - Why make you premium - page when they try to access to a blocked section
    [tags]              GOLDBUSTER-9
    [Documentation]     GOLDBUSTER-9 Why make you premium pagehey try to access to blocked sections - advanced
    Given I login in the application ${AA}   ${PASS}
    When I change of level   advanced
    then I tap on a unit   98
    then Exercises option in the unit should be blocked
    then I tap on Exercises option
    And Why make you premium message should be displayed
    then tap on start tour premium course
    And old plan page should be displayed


new registered users should be able to see the option Why make you premium page when they try to access to a blocked section
    [tags]              GOLDBUSTER-9
    [Documentation]     GOLDBUSTER-9 new users
    Given a new user registered in the app   GOLDBUSTER-9
    When a level test page should be displayed
    Then I select a level  advanced
    then I tap on a unit   98
    then Exercises option in the unit should be blocked
    then I tap on Exercises option
    And Why make you premium message should be displayed
    then tap on start tour premium course
    And old plan page should be displayed





