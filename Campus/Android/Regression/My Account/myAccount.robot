*** Settings ***
Documentation  This file contains tests of the assessment in the Android Application
Resource    ../../config.robot
Library  AppiumLibrary
Library  String

Test Set Up         I start the ABA application
Test Tear Down      close application


*** Test Cases ***
As a premium user, I should not be able to see buttons to go to the plans page in My account
    [Tags]  android    critical   emulator    real    myAccount
    Given I login in the application with premium user
    When I go to My Account area using the menu
    Then I should not see the view prices button in My Account

I should be able to change the password and login with the new one.
    [Tags]  android    medium   emulator    real    myAccount   test1
    Given I create an user and I go to the menu of the level     Beginners
    And I go to My Account area using the menu
    And I click in the change password button
    And I change my password writting new one   ${FREE USER-PASSWORD}   ${PASS}
    And reset application
    And I login in the application  ${RANDOM USER-EMAIL}  ${PASS}
    Then The user is on the main course page

I should be able to change the password twice using My Account
    [Tags]  android    main   emulator    real    myAccount     test1
    [Documentation]     The bug was solved in the ticket: ABAANDROID-739
    Given I create an user and I go to the menu of the level     Beginners
    And I go to My Account area using the menu
    And I click in the change password button
    And I change my password writting new one   ${FREE USER-PASSWORD}   ${FACEBOOK PHONEUSER-EMAIL}
    And I click in the change password button
    And I change my password writting new one   ${FACEBOOK PHONEUSER-EMAIL}   ${PASS}
    And reset application
    And I login in the application  ${RANDOM USER-EMAIL}  ${PASS}
    Then The user is on the main course page

I should be redirected to the list course page when I go back in my account
    [Tags]  android    main   emulator    real    myAccount
    Given I login in the application with premium user
    When I go to My Account area using the menu
    And go back
    Then The user is on the main course page