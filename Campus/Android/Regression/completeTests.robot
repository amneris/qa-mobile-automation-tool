*** Settings ***
Resource    ../config.robot
#Resource    ../../../startDevice.robot
Documentation  This file contains the tests that use different pages of the Mobile Application.
Library  AppiumLibrary
Library  String

#Suite Set Up        I start the ABA application
Test Set Up         I start the ABA application
Test Tear Down      close application
#                        Run Keywords
#...				    Run Keyword If Test Failed    close application   AND
#...                 Run Keyword If Test Failed	get screen shot
#Suite Tear Down	    Reset application

*** Variables ***


*** Test Cases ***
Free user should be able to see the premium button
    [Tags]  android     critical    real    emulator
    Given I login in the application with free user
    When I go to My Account area using the menu
    Then The view prices button should appear in My account

User should be able to go the plan page and see the three plans
    [Tags]  android     critical      real
    Given I login in the application with free user
    When I go to the plan page using the menu
    Then I should see at least two prooducts in the plan page

User should be able to go to the plan pages after he changed his password
    [Tags]  android     medium      real
    [Documentation]     The bug was solved in the ticket: ABAANDROID-739
    Given I go to the creation user page
    And I generate an random email
    And I create an user with following data    ${RANDOM USER-EMAIL}  ${FREE USER-PASSWORD}
    And reset application
    And I login in the application  ${RANDOM USER-EMAIL}  ${FREE USER-PASSWORD}
    When I go to My Account area using the menu
    And I click in the change password button
    And I change my password writting new one   ${FREE USER-PASSWORD}   ${FACEBOOK PHONEUSER-EMAIL}
    And I go to the plan page using the menu
    Then I should see at least two prooducts in the plan page
