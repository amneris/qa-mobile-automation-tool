*** Settings ***
Documentation  This file contains the tests that use different pages of the Mobile Application.
Resource    ../../config.robot
Library  AppiumLibrary

Test Set Up         I start the ABA application
Test Tear Down      close application

*** Test Cases ***
Users should be able to go to Google Play to rate the app from the contact page
    [Tags]  android     critial    real      helpCenter
    Given I login in the application with free user
    When On main menu I tap on  CONTACT AND HELP
    And I go to the Rate the app section
    Then I am on the Google Play story app

Users should be able to go to the Help section from the contact page
    [Tags]  android     medium    real      helpCenter
    Given I login in the application with free user
    When On main menu I tap on  CONTACT AND HELP
    And I go to the Help center section
    Then I am on the Help center page

User should be redirected to the contact and gelp page when he goes back in the help center page
    [Tags]  android     trivial    real      helpCenter
    Given I login in the application with free user
    When On main menu I tap on  CONTACT AND HELP
    And I go to the Help center section
    And I go bock to the contact and help page
    Then I should be in the main page of the contact and help section
