*** Settings ***
Documentation  This file contains the common variables and keywords that are used in IOS and Android application
Library     AppiumLibrary
Library     String


*** Variables ***
# These variables should not be changed by any test.
${FREE USER-EMAIL}                              qa-user-mobile@qa.com
${FREE USER-PASSWORD}                           test123
#${PREMIUM USER-EMAIL}                           student+en@abaenglish.com
#${PREMIUM USER-PASSWORD}                        abaenglish
${PREMIUM USER-EMAIL}                           qavargas@qa.com
${PREMIUM USER-PASSWORD}                        test123
${RANDOM USER-EMAIL}                            random@abaenglish.com
${FACEBOOK EMAILUSER-EMAIL}                     avargas@abaenglish.com
${FACEBOOK PHONEUSER-EMAIL}                     601394144
${VAR-EMAIL-FREE-USER}          qauser@qa.com
${VAR.PASSWORD-FREE-USER}       test123
${VAR-EMAIL-PREMIUM-USER}       student+en@abaenglish.com
${VAR.PASSWORD-PREMIUM-USER}    abaenglish


#######initial page
##${sel-main login already a user button}     id=com.abaenglish.videoclass:id/loginTextView
##${sel-main login register now button}       id=com.abaenglish.videoclass:id/registerButton

########login page
##${sel-login user email field}                   id=com.abaenglish.videoclass:id/emailEditTextInput
##${sel-login user password field}                id=com.abaenglish.videoclass:id/passwordEditTextInput
##${sel-login log-in button}                      id=com.abaenglish.videoclass:id/loginActionButton
##${sel-login forgot password button}             id=com.abaenglish.videoclass:id/loginChangePasswordButton
##${sel-pop-up allow ABA record audio}            id=com.android.packageinstaller:id/permission_allow_button

##${sel-login log-in with facebbok button}        id=com.abaenglish.videoclass:id/loginFacebookButton
##${sel-login log-in facebbok email field}        xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[3]/android.widget.EditText[1]
##${sel-login log-in facebbok password field}     xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[3]/android.widget.EditText[2]
#     xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[3]/android.widget.Button[1]
##${sel-login log-in facebbok confirm button}     xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[1]/android.view.View[2]/android.view.View[2]/android.widget.Button[1]
#${sel-login log-in facebbok confirm button}     id=u_0_2


#######  regist a new user create

##${sel-create user name field}                   id=com.abaenglish.videoclass:id/nameEditTextInput
##${sel-create user email field}                  id=com.abaenglish.videoclass:id/emailEditTextInput
##${sel-create user password field}               id=com.abaenglish.videoclass:id/passwordEditTextInput
##${sel-create user registor now button}          id=com.abaenglish.videoclass:id/registerActionButton
##${sel-create user facebook button}              id=com.abaenglish.videoclass:id/registerFacebookButton
#${sel-create user facebook email field}         xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[3]/android.widget.EditText[1]
##{sel-create user facebook password field}      xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[3]/android.widget.EditText[2]
#${sel-create user log in button}                xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.Button[1]
#${sel-create user log in text}                  id=com.abaenglish.videoclass:id/snackbar_text
#${sel-create user log in button}                id=com.abaenglish.videoclass:id/snackbar_action


####### HELP CENTER######
##${SEL-CONTACT&HELP HELP CENTER BUTTON}          id=com.abaenglish.videoclass:id/zendesk_knowledge
##${SEL-CONTACT&HELP RATE THE APP BUTTON}         id=com.abaenglish.videoclass:id/zendesk_rate

# Selectors of the Help Center area
##${SEL-HELP CENTER GO BACK BUTTON}              id=com.abaenglish.videoclass:id/toolbarLeftButton
#${SEL-HELP CENTER SEARCH FIELD}                 xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.view.View[1]/android.view.View[2]
#${SEL-HELP CENTER EMAIL BUTTON}                 xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.view.View[1]/android.widget.ListView[1]/android.view.View[1]/android.view.View[2]

# Selectors of the Google Play Page
##${SEL-GOOGLE PLAY HEADER}                       id=com.android.vending:id/action_bar_container_container
##${SEL-GOOGLE PLAY UNNISTALL BUTTON}             id=com.android.vending:id/uninstall_button
##${SEL-GOOGLE PLAY OPEN BUTTON}                  id=com.android.vending:id/launch_button

#### COURSES ##############
##${sel-main courses tittle}                          id=com.abaenglish.videoclass:id/toolbarTitle
##${sel-main courses beginners first unit}            xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ExpandableListView[1]/android.widget.LinearLayout[5]
#${sel-main courses beginners first unit progress}   xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ExpandableListView[1]/android.widget.LinearLayout[4]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.TextView[4]

# The following variables has the unit names
${first unit - tittle}                A Day at the Beach
${second unit - tittle}               A New Friend


####### forgot password
##${sel-forgot password email field}          id=com.abaenglish.videoclass:id/changePassword
##${sel-forgot password send button}          id=com.abaenglish.videoclass:id/changePasswordSend
##${sel-forgot password pop-up message}       id=com.abaenglish.videoclass:id/contentPanel
##${sel-forgot password pop-up button}        id=android:id/button1





##### UNIT PAGE


#${sel-unit page download button}                    id=com.abaenglish.videoclass:id/toolbarRightButton
##${sel-unit page permissions button}                 id=com.abaenglish.videoclass:id/snackbar_action
#${sel-unit page back button}                        id=com.abaenglish.videoclass:id/toolbarLeftButton
##${sel-unit page tittle unit}                        id=com.abaenglish.videoclass:id/toolbarTitle
##${sel-unit page tittle progress}                    id=com.abaenglish.videoclass:id/toolbarSubTitle
##${sel-unit page start assessment}                   id=com.abaenglish.videoclass:id/start_evaluation
#${sel-unit page aba film class - rosco}             xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]
${sel-unit page aba film class - lista}             xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[1]
${sel-unit page speaking class - rosco}             xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[4]
${sel-unit page speaking class - lista}             xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[2]
${sel-unit page write class - rosco}                xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[8]
${sel-unit page write class - lista}                xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[3]
${sel-unit page interpret class - rosco}            xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[7]
${sel-unit page interpret class - lista}            xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[4]
${sel-unit page videoclass class - rosco}           xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[6]
${sel-unit page videoclass class - lista}           xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[5]
${sel-unit page exercises class - rosco}            xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[5]
${sel-unit page exercises class - lista}            xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[6]
${sel-unit page vocabulary class - rosco}           xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[2]
${sel-unit page vocabulary class - lista}           xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[7]
${sel-unit page assessment - rosco}                 xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[3]
${sel-unit page assessment - lista}                 xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[8]


# Selectors of the ABA Film and VideoClass
##${sel-aba film page start button}                   id=com.abaenglish.videoclass:id/play_button

#${sel-videos rew button}                            id=android:id/rew
#${sel-videos pause button}                          id=android:id/pause
##${sel-videos advance button}                        id=android:id/ffwd
##${sel-videos subtitles option}                      id=com.abaenglish.videoclass:id/downLayout
##${sel-videos subtitles not active button}           id=com.abaenglish.videoclass:id/subSecondText
##${sel-videos subtitles active button}               id=com.abaenglish.videoclass:id/subThreeText

# Selectors of the speaking class
#${sel-speaking page start button}                   id=com.abaenglish.videoclass:id/centerButtonView

# Selectors of the write class
##${sel-write page play button}                       id=com.abaenglish.videoclass:id/listen_button
#${sel-write page next button}                       id=com.abaenglish.videoclass:id/checkButton
#${sel-write page write textField}                   id=com.abaenglish.videoclass:id/writeEditText

# Selectors of the interpret class
##${sel-interpret first character button}             xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.ListView[1]/android.widget.LinearLayout[1]
#${sel-interpret first character button - view 2do}  xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.ImageView[1]
##${sel-interpret second character button}            xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.ListView[1]/android.widget.LinearLayout[2]
##${sel-interpret start record button}                id=com.abaenglish.videoclass:id/centerButton

# Selectors of the exercises class
##${sel-exercises page check results button}          id=com.abaenglish.videoclass:id/checkAnswersButton

# Selectors of the vocabulary class
##${sel-vocabulary start button}                      id=com.abaenglish.videoclass:id/centerButton

# Selectors of the assessment area
##${sel-assessment question number text}              id=com.abaenglish.videoclass:id/evaluationNumber
#${sel-assessment answers first button}              id=com.abaenglish.videoclass:id/evaluationOption1
##${sel-assessment complete see mistakes button}      id=com.abaenglish.videoclass:id/repeat_evaluation
##${sel-assessment complete next unit button}         id=com.abaenglish.videoclass:id/continue_nextUnit
##${sel-assessment repeat the test button}            id=com.abaenglish.videoclass:id/repeat_evaluation

# Selectos used in the permissions funcionality
##${sel-unit page pop-up deny button}                 id=com.android.packageinstaller:id/permission_deny_button
##${sel-unit page pop-up allow button}                id=com.android.packageinstaller:id/permission_allow_button
#${sel-unit page permissions button}                 id=com.abaenglish.videoclass:id/snackbar_action
##${sel-settings page application information}        id=com.android.settings:id/all_details



### levels-list
#${sel-main level tittle}                    id=com.abaenglish.videoclass:id/top_text
##${sel-main beginners level button}          xpath= //android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[6]

## TThe following variables contains have the name of levels.
${level - beginners}        Beginners



####menu.robot

##${SEL-MENU BUTTON}                      id=com.abaenglish.videoclass:id/toolbarLeftButton
###${SEL-MENU YOUR ACCOUNT BUTTON}         xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[1]
##${SEL-MENU LEVELS BUTTON}               xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[3]
##${SEL-MENU CONTAND AND HELP BUTTON}     xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[6]
##${SEL-MENU VIEW PRICES BUTTON}          id=com.abaenglish.videoclass:id/premiumButton_menu

##### my account
##${SEL-MY ACCOUNT CHANGE PASSWORD - OLD PASSWORD FIELD}      id=com.abaenglish.videoclass:id/oldPasswordEditTextInput
##${SEL-MY ACCOUNT CHANGE PASSWORD - NEW PASSWORD FIELD}      id=com.abaenglish.videoclass:id/newPasswordEditTextInput
##${SEL-MY ACCOUNT CHANGE PASSWORD - NEW 2 PASSWORD FIELD}    id=com.abaenglish.videoclass:id/repeatNewPasswordEditTexInput
#${SEL-MY ACCOUNT CHANGE PASSWORD - CONFIRM BUTTON}          id=com.abaenglish.videoclass:id/changePasswordButton


##${SEL-MY ACCOUNT CHANGE PASSWORD BUTTON}        id=com.abaenglish.videoclass:id/changePassword
#${SEL-MY ACCOUNT RESTORE PURCHASES BUTTON}      id=com.abaenglish.videoclass:id/restorePurchases
##${SEL-MY ACCOUNT LOG OUT BUTTON}                id=com.abaenglish.videoclass:id/logOutButton
${SEL-MY ACCOUNT PREMIUM BUTTON}                id=com.abaenglish.videoclass:id/premiumButton


######  plan page

${sel-payment one-month plan}               xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[1]
${sel-payment six-months plan}              xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[2]
${sel-payment twelve-months plan}           xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[3]
${sel-payment one-month plan button}        xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.Button[1]
${sel-payment six-months plan button}       xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[2]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.Button[1]
${sel-payment twelve-months plan button}    xpath=//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[3]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.Button[1]


##${sel-whitePage first paragraph text}       id=com.abaenglish.videoclass:id/textview_title
##${sel-whitePage view prices button}         id=com.abaenglish.videoclass:id/button_view_prices