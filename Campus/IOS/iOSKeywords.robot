*** Settings ***
Documentation  This file contains the keywords used in the IOs application.
Library  AppiumLibrary

# Files that are called
Resource    ../../users.robot
Resource    ../../globalKeywords.robot

*** Keywords ***
### ------- These keywords are for the Login Pages    ------- ###
#--- Initial Page
I click in the start now button in the start window
    sleep  2s
    I close the notifications pop-up if it exists
    wait until page contains element    xpath=//XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
    click element     xpath=//XCUIElementTypeOther[1]/XCUIElementTypeButton[1]

I click in the log-in button in the start window
    sleep  2s
    I close the notifications pop-up if it exists
    wait until page contains element    xpath=//XCUIElementTypeOther[1]/XCUIElementTypeButton[2]
    click element     xpath=//XCUIElementTypeOther[1]/XCUIElementTypeButton[2]

I close the notifications pop-up if it exists
    ${passed} =	    Run Keyword And Return Status	page should contain element     xpath=//XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeButton[1]
    Run Keyword If	${passed} == True    I click in the allow button in the pop-up notification

I click in the allow button in the pop-up notification
    click element   xpath=//XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeButton[1]

I should be in initial window of the application
    [Documentation]     Improve
    element should be enabled       xpath=//XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
    element should be enabled       xpath=//XCUIElementTypeOther[1]/XCUIElementTypeButton[2]

#--- Login page
I login in the applicatiom with free user
    I click in the log-in button in the start window
    I login in the applicatiom  ${FREE USER-EMAIL}      ${FREE USER-PASSWORD}
#    I login in the applicatiom    qastudent+aa@abaenglish.com  B1gB4ngT3st!

I login in the applicatiom with premium user
    I click in the log-in button in the start window
    I login in the applicatiom  ${PREMIUM USER-EMAIL}   ${PREMIUM USER-PASSWORD}
#    I login in the applicatiom      qastudent+esp@abaenglish.com    B1gB4ngT3st!

I login in the applicatiom
    [Arguments]  ${email}   ${password}
    I click in the log-in button in the start window
    I fill the user email field in the login        ${email}
    I fill the user password field in the login     ${password}
    I click in the log-in button
    I close the access the microphone pop-up

I fill the user email field in the login
    [Arguments]  ${email}
    wait until page contains element  xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
    Clear Text  xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
    input text  xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]       ${email}

I fill the user password field in the login
    [Arguments]  ${password}
    Clear Text  xpath=//XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField[1]
    input password  xpath=//XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField[1]    ${password}

I click in the log-in button
    wait until page contains element    name=LOG IN
    click element  name=LOG IN

I click in the forgot password button
    wait until page contains element    xpath=//XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[3]
    click element   xpath=//XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[3]

The user is in the log-in page
    [Documentation]     Improve
    wait until page contains element    xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
    element should be enabled           xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]

I close the access the microphone pop-up
    [Documentation]     Improve
    sleep  7s
    ${passed} =	    Run Keyword And Return Status	page should contain text  OK
    Run Keyword If	${passed} == True    click text  OK


#--- Create user page
I go to the creation user page
    I click in the start now button in the start window

I create an user and I go to the menu of the level
    [Arguments]     ${level}
    I create an user and I select the level     ${level}
    wait until page contains element    xpath=//XCUIElementTypeNavigationBar[1]/XCUIElementTypeButton[1]
    click element   xpath=//XCUIElementTypeNavigationBar[1]/XCUIElementTypeButton[1]

I create an user and I select the level
    [Arguments]     ${level}
    I go to the creation user page
    I create a user with random email
    log     ${level}
    click element   xpath=//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[2][contains(@value, ${level})]
    I close the access the microphone pop-up

I create an accont with the free user email
    I create an user with following data    ${FREE USER-EMAIL}    test12345

I create a user with random email
    I generate an random email
    I create an user with following data    ${RANDOM USER-EMAIL}    ${FREE USER-PASSWORD}

I create an user with following data
    [Arguments]     ${user-email}    ${user-password}
    wait until page contains element    xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
    I fill the user name field in creation account      qa-test
    I fill the user email field in creation account     ${user-email}
    I fill the user password field in creation account  ${user-password}
    I click in the registor now button
    sleep  2s

I fill the user name field in creation account
    [Arguments]     ${user-name}
    clear text  xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
    input Text  xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]       ${user-name}

I fill the user email field in creation account
    [Arguments]     ${user-email}
    clear text  xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[2]
    input Text  xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[2]      ${user-email}

I fill the user password field in creation account
    [Arguments]     ${user-password}
    input Text      xpath=//XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField[1]  ${user-password}

I click in the registor now button
    click element   xpath=//XCUIElementTypeOther[1]/XCUIElementTypeButton[2]

The user is in the creation user page
    [Documentation]     Improve
    wait until page contains element    xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
    element should be enabled           xpath=//XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]

#- These keywords are for the pop-up when the user is registered in the database.
I click in the pop-up the option forgot my password
    wait until page contains element    name=I forgot my password
    click element   name=I forgot my password

I click in the pop-up the option log-in
    wait until page contains element    name=Log in
    click element   name=Log in

I click in the pop-up the option ok
    wait until page contains element    name=OK
    click element   name=OK

#--- Forgot password: page
I go to the recover password page
    I click in the log-in button in the start window
    I click in the forgot password button

I recover the password for the email
    [Arguments]  ${email}
    I fill the email to recover the password       ${email}
    I click in the recover password button

I fill the email to recover the password
    [Arguments]  ${email}
    wait until page contains element    xpath=///XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
    input text  xpath=///XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]  ${email}

I click in the recover password button
    tap  name=SEND

The pop-up to recover the password has valid message
    [Documentation]     Improve
    wait until page contains element    xpath=//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeAlert[1]
    element should be enabled           xpath=//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeAlert[1]
#    Element Should Contain Text     xpath=//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeAlert[1]      We have sent you an e-mail

The pop-up to recover the password can be closed
    click element  name=OK
    The user is in the forgot password page

The user is in the forgot password page
    [Documentation]     Improve
    wait until page contains element    xpath=///XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
    element should be enabled           xpath=///XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]


### ------- These keywords are for the Campus sections    ------- ###
#- Levels
I should be in the select level after registration
    [Documentation]     The user only access to this page after an account is created.
    Set Test Variable   ${variable-name}    levelSelectionView
    wait until page contains element    xpath=//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1][contains(@name, ${variable-name})]
    element should be enabled           xpath=//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1][contains(@name, ${variable-name})]

#- Menu
I select in the menu
   [Arguments] 	${MAIN-MENU-OPTION}
   I click in the menu icon
   click element   name=${MAIN-MENU-OPTION}

I select in the menu the My Account option
   I click in the menu icon
   click element    xpath=///XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]

I click in the menu icon
    wait until page contains element    xpath=//XCUIElementTypeNavigationBar[1]/XCUIElementTypeButton[1]
    click element                       xpath=//XCUIElementTypeNavigationBar[1]/XCUIElementTypeButton[1]

I should see the menu icon
    wait until page contains element    xpath=//XCUIElementTypeNavigationBar[1]/XCUIElementTypeButton[1]
    element should be enabled           xpath=//XCUIElementTypeNavigationBar[1]/XCUIElementTypeButton[1]

I should not see the buttons to go to the plans page
    Page should not contain element         name=GAIN FULL ACCESS
    Page should not contain element         name=View prices

#- My Account
I log-out using my account
    I select in the menu the My Account option
    scroll          name=CHANGE PASSWORD     name=Logout
    click element   name=Logout

I click in the plans page butoon in My account
    wait until page contains element    name=SEE PRICES
    click element                       name=SEE PRICES

I should not see the view prices button in My Account
    Page should not contain element     name=SEE PRICES

I go to the change password are in My Account
    I select in the menu the My Account option
    I click in the change password in My Account

I click in the change password in My Account
    click element   name=CHANGE PASSWORD

I change my password writting new one
    [Arguments]  ${old-password}    ${new-password}
    I select in the menu the My Account option
    I click in the change password in My Account
    input password      xpath=//XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField[1]   ${old-password}
    input password      xpath=//XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField[2]   ${new-password}
    input password      xpath=//XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField[3]   ${new-password}
    Run Keyword And Return Status   Hide Keyboard
    click element       name=CHANGE PASSWORD
    sleep  2s

#- Course list
I should be in the course list of the Level
    [Documentation]     The keywords confirm with the tab information the name of the
    [Arguments]  ${section}
    wait until page contains element    xpath=//XCUIElementTypeNavigationBar
    element should be enabled           xpath=//XCUIElementTypeNavigationBar[contains(@text, ${section})]

#- Plan page
I should see the three products in the plan page
    I am in the plan page
    The plan page contains the product  1 month
    The plan page contains the product  6 months
    The plan page contains the product  12 months

I am in the plan page
    wait until page contains element    name=Special prices for the app!
    element should be enabled   name=Special prices for the app!

The plan page contains the product
    [Arguments]     ${product}
    element should be enabled   ${product}