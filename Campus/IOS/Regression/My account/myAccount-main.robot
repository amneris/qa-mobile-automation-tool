*** Settings ***
Documentation   The file contains the test cases of the My account section including the functionality of change password.
Library         AppiumLibrary    run_on_failure=Log Source

# Files that are called
Resource    ../../../../startDevice.robot
Resource    ../../iOSKeywords.robot

# Pre-conditions activities executed to all tests in this file.
Test Set Up     Run Keywords
...             I start the ABA application

*** Test Cases ***
User should be able to change the password twice using My Account
    [Tags]  ios     medium    real    myAccount
    Given I create an user and I go to the menu of the level     Intermediateddd
    When I change my password writting new one   ${FREE USER-PASSWORD}   ${PASS}
    And I change my password writting new one   ${PASS}    ${FACEBOOK PHONEUSER-EMAIL}
    And I log-out using my account
    Then I login in the applicatiom  ${RANDOM USER-EMAIL}  ${FACEBOOK PHONEUSER-EMAIL}
    And I should see the menu icon

As a premium user, I should not be able to see buttons to go to the plans page in My account
    [Tags]  ios     critical    real      myAccount    planPage
    Given I login in the applicatiom with premium user
    When I select in the menu the My Account option
    Then I should not see the view prices button in My Account