*** Settings ***
Documentation   The file contains the test cases of menu
Library         AppiumLibrary    run_on_failure=Log Source

# Files that are called
Resource    ../../../../startDevice.robot
Resource    ../../iOSKeywords.robot

# Pre-conditions activities executed to all tests in this file.
Test Set Up     Run Keywords
...             I start the ABA application

*** Test Cases ***
As a premium user, I should not be able to see buttons to go to the plans page in the menu section
    [Tags]  ios     critical      real      menu    planPage
    Given I login in the applicatiom with premium user
    When I click in the menu icon
    Then I should not see the buttons to go to the plans page