*** Settings ***
Documentation   The file contains the test cases of the login pages: initial page, login page, create user page and forgot password page
Library         AppiumLibrary    run_on_failure=Log Source

# Files that are called
Resource    ../../../../startDevice.robot
Resource    ../../iOSKeywords.robot

# Pre-conditions activities executed to all tests in this file.
Test Set Up     Run Keywords
...             I start the ABA application

*** Test Cases ***
# ------------- Test cases are of the creation account functionality. ---------------------
I should be able to create an account using IOS application
    [Tags]  ios     critical    real    loginPages  createUser
     Given I go to the creation user page
     When I create a user with random email
     Then I should be in the select level after registration

I should be redirected to the log-in page after I select forgot password option in the creation account pop-up.
    [Tags]  ios     trivial     real    loginPages
    Given I go to the creation user page
    When I create an accont with the free user email
    And I click in the pop-up the option forgot my password
    Then The user is in the forgot password page

I should be redirected to the log-in page after I select log-in option in the creation account pop-up.
    [Tags]  ios     trivial     real    loginPages
    Given I go to the creation user page
    When I create an accont with the free user email
    And I click in the pop-up the option log-in
    Then The user is in the log-in page

I should stay in the creation page after I select the ok option in the pop-up
    [Tags]  ios     trivial     real    loginPages
    Given I go to the creation user page
    When I create an accont with the free user email
    And I click in the pop-up the option ok
    Then The user is in the creation user page


# ------------- Test cases are of the recover password functionality. ---------------------
The user should be able to recover his password
    [Tags]  ios    critical    real     loginPages  recoverPassword
    Given I go to the recover password page
    When I recover the password for the email    ${FREE USER-EMAIL}
    Then The pop-up to recover the password has valid message
    And The pop-up to recover the password can be closed