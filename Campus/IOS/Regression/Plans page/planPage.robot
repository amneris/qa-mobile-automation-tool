*** Settings ***
Documentation   The file contains the test cases of the plan page and the different ways how users can arrive to this page.
Library         AppiumLibrary    run_on_failure=Log Source

# Files that are called
Resource    ../../../../startDevice.robot
Resource    ../../iOSKeywords.robot

# Pre-conditions activities executed to all tests in this file.
Test Set Up     Run Keywords
...             I start the ABA application

*** Test Cases ***
As a free user, I should be able to go to the plans page using the button that is on the bottom
    [Tags]  ios     critical      real      menu    planPage
    Given I login in the applicatiom with free user
    When I select in the menu   View prices
    Then I should see the three products in the plan page

As a free user, I should be able to go to the plans page using the menu button
    [Tags]  ios     medium      real      menu    planPage
    Given I login in the applicatiom with free user
    When I select in the menu   GAIN FULL ACCESS
    Then I should see the three products in the plan page

As a free user, I should be able to go to the plans from My Account
    [Tags]  ios     medium      real      menu    planPage
    Given I login in the applicatiom with free user
    When I select in the menu the My Account option
    And I click in the plans page butoon in My account
    Then I should see the three products in the plan page

User should be able to go to the plan pages after he changed his password
    [Tags]  ios     medium      real
    [Documentation]     The bug was solved in the ticket: ABAIOS-790
    Given I create an user and I go to the menu of the level     Intermediateddd
    When I change my password writting new one   ${FREE USER-PASSWORD}   ${PASS}
    And I click in the plans page butoon in My account
    Then I should see the three products in the plan page

