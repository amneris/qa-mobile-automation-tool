*** Settings ***
Documentation   The file contains all the test cases related with the course section
Library         AppiumLibrary    run_on_failure=Log Source

# Files that are called
Resource    ../../../../startDevice.robot
Resource    ../../iOSKeywords.robot

# Pre-conditions activities executed to all tests in this file.
Test Set Up     Run Keywords
...             I start the ABA application

*** Test Cases ***