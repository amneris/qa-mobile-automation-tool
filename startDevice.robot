*** Settings ***
Documentation  This file contains the tests of the Login Page in the Android Application
Library  AppiumLibrary
Library  String

*** Variables ***
# These variables never change for Android and IOS devices.
${URL}                                      http://localhost:4723/wd/hub
${ANDROID-PLATFORM}                         Android
${ANDROID-APP}                              /Users/walex/Documents/workspace/aba-android/aba-android/app/build/outputs/apk/app-internal-debug.apk
#${ANDROID-APP}                              /Users/alejandrovargas/Documents/repositories/aba-android/app/build/outputs/apk/app-internal-debug.apk
${ANDROID-PACKAGE}                          com.abaenglish.videoclass
${ANDROID-ACTIVITY}                         com.abaenglish.videoclass.SplashActivity

${IOS-PLATFORM_NAME}                        iOS
${IOS-APP}                                  /Users/alejandrovargas/Library/Developer/Xcode/DerivedData/ABA-hfthngnchwhjnhbqcpzacdicrakj/Build/Products/Debug-iphoneos/ABA.app

# These variables of Android change depending the device used, these are update in the keyword: Set variables for Android Devices
${ANDROID-VERSION}                          5.0.2
${ANDROID-DEVICE}                           91dda0a0

# These variables of IOS change depending the device used, these are update in the keyword: Set variables for iOS Devices
${IOS-PLATFORM_VERSION}                     10
${IOS-DEVICE_NAME}                          a
${IOS-UDID}                                 a

# Variables used to detect the device used in the test and thier platform, these are used it to execute the test in Android and Ios
${DEVICE}                                   deviceUsedForTheTest
${DEVICE-PLATFORM}                          devicePlatformIOSorAndroid

# Information of Android devices used
${AND-REAL-DEVICE-EMULATOR}                 Android Emulator
${AND-REAL-VERSION-EMULATOR}
${AND-REAL-DEVICE-HUAWEI-V19}               7N2NEF148S024011
${AND-REAL-VERSION-HUAWEI-V19}              4.4
${AND-REAL-DEVICE-SAMSUNG-WHITE-V21}        558fa17c
${AND-REAL-VERSION-SAMSUNG-WHITE-V21}       5.0.1
${AND-REAL-DEVICE-SAMSUNG-V21}              91dda0a0
${AND-REAL-VERSION-SAMSUNG-V21}             5.0.2
${AND-REAL-DEVICE-TABLET-AMAZON-V22}        G0K0H404535707NN
${AND-REAL-VERSION-TABLET-AMAZON-V22}       5.1
${AND-REAL-DEVICE-SONY-ALEJO-V23}           CB5A25H8UF
${AND-REAL-VERSION-SONY-ALEJO-V23}          6.0.1
${AND-REAL-DEVICE-TABLET-NEXUS-V23}         079b91f7
${AND-REAL-VERSION-TABLET-NEXUS-V23}        6.0.1

# Information of Ios devices used
${IOS-REAL-PLATFORM-VERSION-IPHONE-5}       10
${IOS-REAL-DEVICE-NAME-IPHONE-5}            iPhone 5c ABA English
${IOS-REAL-DEVICE-UDID-IPHONE-5}            4d333f04ba5f74fe5cee05cc1f1bbca4a9a674e2
${IOS-REAL-PLATFORM-VERSION-IPHONE-6}       10
${IOS-REAL-DEVICE-NAME-IPHONE-6}            ABA iPhone 6+
${IOS-REAL-DEVICE-UDID-IPHONE-6}            2278108058c437733f15e1aace8b18cec6c9d6a1
${IOS-REAL-PLATFORM-VERSION-IPHONE-7}       10
${IOS-REAL-DEVICE-NAME-IPHONE-7}            iPhone 7 Plus ABA English
${IOS-REAL-DEVICE-UDID-IPHONE-7}            33fd036867c39ec04da71dade078a9c9091fed7e

*** Keywords ***
I start the ABA application
    Set variables to execute the test
    ${passed} =	    Run Keyword And Return Status   should be equal     ${DEVICE-PLATFORM}      Android
    Run Keyword If	${passed} == True    I use an Android device
    Run Keyword If	${passed} == False   I use an IOS device

I use an Android device
    [Documentation]  This keyword starts the application in an Android device or emulator.
    open application  ${URL}  platformName=${ANDROID-PLATFORM}  	platformVersion=${ANDROID-VERSION}  deviceName=${ANDROID-DEVICE}  app=${ANDROID-APP}  appPackage=${ANDROID-PACKAGE}   appActivity=${ANDROID-ACTIVITY}

I use an IOS device
    [Documentation]  This keyword starts the application in an IOS device or emulator.
    open application    ${URL}    platformName=${IOS-PLATFORM_NAME}     platformVersion=${IOS-PLATFORM_VERSION}     deviceName=${IOS-DEVICE_NAME}      app=${IOS-APP}   udid=${IOS-UDID}
    sleep   10s
    I click in allow button when it appears

Set variables to execute the test
    [Documentation]     This keyword uses the variable "device" sent by the user in the command line to detects the phone or emulator and calls the Android or Ios
    ...                 keyword to set the correct values to execute the test.

# ----------- FIRST IS TESTED TO DETECT IF THE DEVICE USED IS ANDROID -----------------------------
    # Compare the variable to know if the device used is an Android emulator with version 25.
    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   EMULATOR
    Run Keyword If	${passed} == True   Set variables for Android Devices   ${AND-REAL-DEVICE-EMULATOR}   ${AND-REAL-VERSION-EMULATOR}

    # Compare the variable to know if the device used is the Samsung v21 color white.
    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   HUAWEI
    Run Keyword If	${passed} == True   Set variables for Android Devices   ${AND-REAL-DEVICE-HUAWEI-V19}   ${AND-REAL-VERSION-HUAWEI-V19}

    # Compare the variable to know if the device used is the Samsung v21 color white.
    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   SAMSUNG-WHITE
    Run Keyword If	${passed} == True   Set variables for Android Devices   ${AND-REAL-DEVICE-SAMSUNG-WHITE-V21}   ${AND-REAL-VERSION-SAMSUNG-WHITE-V21}

    # Compare the variable to know if the device used is the Samsung v21 color grey.
    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   SAMSUNG-GREY
    Run Keyword If	${passed} == True   Set variables for Android Devices   ${AND-REAL-DEVICE-SAMSUNG-V21}   ${AND-REAL-VERSION-SAMSUNG-V21}

    # Compare the variable to know if the device used is the tablet Amazon v22 color black.
    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   TABLET-AMAZON
    Run Keyword If	${passed} == True   Set variables for Android Devices   ${AND-REAL-DEVICE-TABLET-AMAZON-V22}   ${AND-REAL-VERSION-TABLET-AMAZON-V22}

    # Compare the variable to know if the device used is the Alejo´s Sony v23 color black.
    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   SONY-ALEJO
    Run Keyword If	${passed} == True   Set variables for Android Devices   ${AND-REAL-DEVICE-SONY-ALEJO-V23}   ${AND-REAL-VERSION-SONY-ALEJO-V23}

    # Compare the variable to know if the device used is the tablet Amazon v22 color black.
    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   TABLET-NEXUS
    Run Keyword If	${passed} == True   Set variables for Android Devices   ${AND-REAL-DEVICE-TABLET-NEXUS-V23}   ${AND-REAL-VERSION-TABLET-NEXUS-V23}


# ----------- THEN IS TESTED TO DETECT IF THE DEVICE USED IS IOS -----------------------------
    # Compare the variable to know if the device used is an iPhone 5. (black color)
    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   IPHONE-5
    Run Keyword If	${passed} == True   Set variables for iOS Devices   ${IOS-REAL-PLATFORM-VERSION-IPHONE-5}   ${IOS-REAL-DEVICE-NAME-IPHONE-5}    ${IOS-REAL-DEVICE-UDID-IPHONE-5}

    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   IPHONE-6
    Run Keyword If	${passed} == True   Set variables for iOS Devices   ${IOS-REAL-PLATFORM-VERSION-IPHONE-6}   ${IOS-REAL-DEVICE-NAME-IPHONE-6}    ${IOS-REAL-DEVICE-UDID-IPHONE-6}

    # Compare the variable to know if the device used is an iPhone 7. (black color)
    ${passed} =	    Run Keyword And Return Status   Should be equal     ${DEVICE}   IPHONE-7
    Run Keyword If	${passed} == True   Set variables for iOS Devices   ${IOS-REAL-PLATFORM-VERSION-IPHONE-7}   ${IOS-REAL-DEVICE-NAME-IPHONE-7}    ${IOS-REAL-DEVICE-UDID-IPHONE-7}

Set variables for Android Devices
    [Documentation]  This keyword updates the suite variables with the Android phone information or emulator used for the test.
    ...     device:  The device name of the real phone or 'Android Emulator' for emulators of the Android Studio.
    ...     version: The device version of the real phone, In emulators of the Android Studio this field is empty.
    [Arguments]    ${device}    ${version}
    Set suite variable  ${ANDROID-DEVICE}       ${device}
    Set suite variable  ${ANDROID-VERSION}      ${version}
    Set suite variable  ${DEVICE-PLATFORM}      Android

Set variables for iOS Devices
    [Documentation]  This keyword updates the suite variables with the IOS phone information used for the test.
    ...     platform-version:  The phone version
    ...     device-name: The name of the phone, it has to be added complete.
    ...     device-udid: The unique udid of the phone, it can be taken from the Xcode in the devices option
    [Arguments]     ${platform-version}     ${device-name}       ${device-udid}
    Set suite variable  ${IOS-PLATFORM_VERSION}     ${platform-version}
    Set suite variable  ${IOS-DEVICE_NAME}          ${device-name}
    Set suite variable  ${IOS-UDID}                 ${device-udid}
    Set suite variable  ${DEVICE-PLATFORM}          Ios

I click in allow button when it appears
    ${passed} =	    Run Keyword And Return Status	page should contain text    Allow
    Run Keyword If	${passed} == True   click text  Allow