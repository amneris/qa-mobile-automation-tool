*** Settings ***
Documentation   This file contains the keywords that are used in Android and IOs applications.
Library     AppiumLibrary
Library     String
Library     OperatingSystem
Library     Collections

*** Variables ***


*** Keywords ***
I generate an random email
    ${email-random} =	Generate Random String	5	[LOWER]
    ${new-email} =	Catenate	SEPARATOR=   qa-mob-	${email-random}	    @abaenglish.com
    set global variable     ${RANDOM USER-EMAIL}    ${new-email}

The element should be visible
    [Documentation]  This keyword waits until an element appears in the screen. If the element does not appear, the keyword fails.
    [Arguments]  ${element}
    wait until page contains element    ${element}
    page should contain element         ${element}

The page should contain the text
    [Documentation]  This keyword waits until the text appears in the screen. If the text does not appear, the keyword fails.
    [Arguments]  ${text}
    wait until page contains        ${text}
    page should contain text        ${text}

I click if the element is visible
    [Documentation]  I click only if the element is visible. When the element does not exist the step is missing, this keyword is missed (not fail the test).
    [Arguments]  ${element}
    ${value}    Run Keyword And Return Status    The element should be visible    ${element}
    Run keyword if    ${value} == True    tap   ${element}

I wait until the element does not contain the text
    [Documentation]  This keywords waits the time desired by the user until an elements appear, if it does not appear the test fail.
    ...     text:       The text that should appear in the screen
    ...     amount:     The amount of times that the keyword checks that the text appears
    ...     sleep-time: The period od time that the keyword will wait between each validation
    [Arguments]  ${text}    ${amount}   ${sleep-time}
    : FOR    ${i}    IN RANGE    0    ${amount}
    \    ${value}    Run Keyword And Return Status    page should contain text    ${text}
    \    Run keyword if    ${value} == False    Exit For Loop
    \    ${i}    Set Variable    ${i}+1
    \    sleep  ${sleep-time}

I wait until the page does not contain the element
    [Documentation]  This keywords waits the time desired by the user until an elements appear, if it does not appear the test fail.
    ...     element:    The element that should appear in the screen
    ...     sleep-time: The period od time that the keyword will wait between each validation
    [Arguments]  ${element}    ${sleep-time}
    : FOR    ${i}    IN RANGE    0    10
    \    ${value}    Run Keyword And Return Status    The element should be visible    ${element}
    \    Run keyword if    ${value} == False    Exit For Loop
    \    ${i}    Set Variable    ${i}+1
    \    sleep  ${sleep-time}

#- Keywords used to read or use file information
I read information of the file
    [Documentation]     The keyword returns in list all the lines saved in the file.
    ...     file: The rote where the file is saved.
    [Arguments]     ${file}     ${lines-deleted}
    ${FILE_CONTENT}=   Get File    ${file}
    @{LINES}=    Split To Lines    ${FILE_CONTENT}
#    Remove From List    ${LINES}    0
    [Return]    @{LINES}

I save the value of a list with split values and values comparison
    [Documentation]     The keyword split the lines of the list, then it compares the value of one position and returns the value of other position
    ...     value-to-compare: The value that is searched.
    ...     column-compared: In the column where is the value searched.
    ...     column-returned: The column that should be returned when the value searched is fount.
    ...     split-character: The characters used to split each line of the list
    ...     lines: List that will be used.
    [Arguments]     ${QUESTION-ID}  ${column-compared}  ${column-returned}  ${split-character}  @{LINES}
    log     ${QUESTION-ID}
    : FOR    ${LINE}    IN    @{LINES}
    \    @{COLUMNS}=            Split String    ${LINE}    separator=${split-character}
    \    ${QUESTION-CODE}=      Get From List    ${COLUMNS}    ${column-compared}
    \    ${VALID-ANSWER}=       Get From List    ${COLUMNS}    ${column-returned}
    \    ${status} =	    Run Keyword And Return Status    Should Be Equal  ${QUESTION-ID}  ${QUESTION-CODE}
    \   Run Keyword If	${status} == True   Exit For Loop
    [Return]   ${VALID-ANSWER}